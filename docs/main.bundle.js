webpackJsonp(["main"],{

/***/ "../../../../../src lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__desktop_desktop_component__ = __webpack_require__("../../../../../src/app/desktop/desktop.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: 'demo',
        component: __WEBPACK_IMPORTED_MODULE_2__desktop_desktop_component__["a" /* DesktopComponent */]
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forRoot(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n<div *ngIf=\"showIntro()\">\n  <article class=\"pa3 pa5-ns\">\n    <h1 class=\"f3 f1-m f-headline-l\">428-desktop</h1>\n    <p class=\"measure lh-copy\">\n      This is a project by Nagee and John for CSC428: Human Computer Interaction at the University of Toronto.\n    </p>\n    <p class=\"measure lh-copy\">\n      By clicking \"Start\", you agree to partecipate in this research project. No personal information will be collected.\n    </p>\n    <p class=\"measure lh-copy mb5\">\n      For more information, please reach out to\n      <a href=\"mailto:john.axon@mail.utoronto.ca\">John</a> or\n      <a href=\"mailto:isthisnagee@gmail.com\">Nagee</a>.\n    </p>\n    <a routerLink=\"/demo\" class=\"f5 no-underline black bg-animate hover-bg-black hover-white inline-flex items-center pa3 ba border-box\">\n      <span class=\"pr1\">Start</span>\n      <svg class=\"w1\" data-icon=\"chevronRight\" viewBox=\"0 0 32 32\" style=\"fill:currentcolor\">\n        <title>chevronRight icon</title>\n        <path d=\"M12 1 L26 16 L12 31 L8 27 L18 16 L8 5 z\"></path>\n      </svg>\n    </a>\n  </article>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
        this.title = 'csc428';
    }
    AppComponent.prototype.showIntro = function () {
        return this.router.url === '/';
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object])
    ], AppComponent);
    return AppComponent;
    var _a;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__desktop_desktop_module__ = __webpack_require__("../../../../../src/app/desktop/desktop.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_notifications__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_modialog__ = __webpack_require__("../../../../ngx-modialog/bundle/ngx-modialog.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_modialog_plugins_js_native__ = __webpack_require__("../../../../ngx-modialog/plugins/js-native/bundle/ngx-modialog-js-native.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';






var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_3__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__desktop_desktop_module__["a" /* DesktopModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                // NgbModule.forRoot(),
                __WEBPACK_IMPORTED_MODULE_7_ngx_modialog__["c" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8_ngx_modialog_plugins_js_native__["a" /* JSNativeModalModule */],
                __WEBPACK_IMPORTED_MODULE_6_angular2_notifications__["SimpleNotificationsModule"].forRoot()
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/desktop.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".grid {\n  height: 95vh;\n  display: -ms-grid;\n  display: grid;\n  grid-template-areas: 'desktop' 'dock';\n  -ms-grid-rows: auto max-content;\n      grid-template-rows: auto max-content;\n}\n.DesktopGrid {\n  width: 100%;\n}\n.DesktopNav {\n  /* height: 5vh; */\n  width: 100%;\n}\n\n.DesktopRight {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-flow: column;\n          flex-flow: column;\n  -webkit-box-flex: 0;\n      -ms-flex: 0 1 50%;\n          flex: 0 1 50%;\n  height: 95vh;\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  border: 1px solid black;\n}\n\n.DesktopRightTop,\n.DesktopRightBottom {\n  -webkit-box-flex: 0;\n      -ms-flex: 0 0 100%;\n          flex: 0 0 100%;\n  height: 50%;\n  max-height: 50%;\n  min-height: 50%;\n  width: 100%;\n  max-width: 100%;\n  overflow: scroll;\n}\n\n.DesktopNavButton {\n  /* margin-right: 0.5rem;\n  margin-top: 0.2rem;\n  */\n  padding: 0.25rem;\n  display: inline-block;\n  box-shadow: black 1px 1px 1px;\n  cursor: pointer;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/desktop/desktop.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"grid\">\n  <div class=\"DesktopGrid cf\">\n    <div class=\"ba b--light-green fl h-100 w-100 w-50-ns\">\n      <csc428-file-explorer></csc428-file-explorer>\n    </div>\n    <div class=\"fl h-100 w-100 w-50-ns\">\n      <div class=\"DesktopRight\">\n        <div class=\"DesktopRightTop\">\n          <csc428-pdf-viewer></csc428-pdf-viewer>\n        </div>\n        <div class=\"DesktopRightBottom \">\n          <csc428-text-viewer></csc428-text-viewer>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div>\n  <div class=\"dib pt2 pl2\">\n    <i (click)=\"toggle()\" *ngIf=\"debug\" class=\"green pointer fa fa-toggle-on\" aria-hidden=\"true\"></i>\n    <i (click)=\"toggle()\" *ngIf=\"!debug\" class=\"pointer fa fa-toggle-off\" aria-hidden=\"true\"></i>\n    <span [class.green]=\"debug === true\" class=\"pt2 pl2\">debug</span>\n  </div>\n  <a hrer=\"https://github.com/isthisnagee/desktop\" class=\"mt2 pr3 fr link dim\">github</a>\n  <p class=\"pl2\" [innerHTML]=\"debugText\">\n  </p>\n</div>\n<csc428-file-selector></csc428-file-selector>"

/***/ }),

/***/ "../../../../../src/app/desktop/desktop.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DesktopComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_debug_service__ = __webpack_require__("../../../../../src/app/desktop/services/debug.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DesktopComponent = (function () {
    function DesktopComponent(debugService) {
        this.debugService = debugService;
    }
    DesktopComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.debugService.currentOutput.subscribe(function (t) { return (_this.debugText = t); });
        this.debugService.currentDebugValue.subscribe(function (d) { return (_this.debug = d); });
    };
    DesktopComponent.prototype.toggle = function () {
        this.debugService.toggle();
    };
    DesktopComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-desktop',
            template: __webpack_require__("../../../../../src/app/desktop/desktop.component.html"),
            styles: [__webpack_require__("../../../../../src/app/desktop/desktop.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_debug_service__["a" /* DebugService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_debug_service__["a" /* DebugService */]) === "function" && _a || Object])
    ], DesktopComponent);
    return DesktopComponent;
    var _a;
}());

//# sourceMappingURL=desktop.component.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/desktop.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DesktopModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular_pipes__ = __webpack_require__("../../../../angular-pipes/esm/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__desktop_component__ = __webpack_require__("../../../../../src/app/desktop/desktop.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__file_explorer_file_explorer_component__ = __webpack_require__("../../../../../src/app/desktop/file-explorer/file-explorer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__text_viewer_text_viewer_component__ = __webpack_require__("../../../../../src/app/desktop/text-viewer/text-viewer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pdf_viewer_pdf_viewer_component__ = __webpack_require__("../../../../../src/app/desktop/pdf-viewer/pdf-viewer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__video_viewer_video_viewer_component__ = __webpack_require__("../../../../../src/app/desktop/video-viewer/video-viewer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__file_selector_file_selector_component__ = __webpack_require__("../../../../../src/app/desktop/file-selector/file-selector.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__file_explorer_new_file_component__ = __webpack_require__("../../../../../src/app/desktop/file-explorer/new-file.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services__ = __webpack_require__("../../../../../src/app/desktop/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_file_selector_service__ = __webpack_require__("../../../../../src/app/desktop/services/file-selector.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














// import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
var DesktopModule = (function () {
    function DesktopModule() {
    }
    DesktopModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_3_angular_pipes__["a" /* NgPipesModule */], __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */]],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__desktop_component__["a" /* DesktopComponent */],
                __WEBPACK_IMPORTED_MODULE_6__file_explorer_file_explorer_component__["a" /* FileExplorerComponent */],
                __WEBPACK_IMPORTED_MODULE_7__text_viewer_text_viewer_component__["a" /* TextViewerComponent */],
                __WEBPACK_IMPORTED_MODULE_8__pdf_viewer_pdf_viewer_component__["a" /* PdfViewerComponent */],
                __WEBPACK_IMPORTED_MODULE_9__video_viewer_video_viewer_component__["a" /* VideoViewerComponent */],
                __WEBPACK_IMPORTED_MODULE_10__file_selector_file_selector_component__["a" /* FileSelectorComponent */],
                __WEBPACK_IMPORTED_MODULE_11__file_explorer_new_file_component__["a" /* NewFileFormComponent */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__desktop_component__["a" /* DesktopComponent */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__services__["c" /* FilesService */],
                __WEBPACK_IMPORTED_MODULE_12__services__["h" /* VideoViewerService */],
                __WEBPACK_IMPORTED_MODULE_12__services__["g" /* TextViewerService */],
                __WEBPACK_IMPORTED_MODULE_12__services__["d" /* PdfViewerService */],
                __WEBPACK_IMPORTED_MODULE_12__services__["f" /* SequitorService */],
                __WEBPACK_IMPORTED_MODULE_13__services_file_selector_service__["a" /* FileSelectorService */],
                __WEBPACK_IMPORTED_MODULE_12__services__["a" /* DebugService */]
            ]
        })
    ], DesktopModule);
    return DesktopModule;
}());

//# sourceMappingURL=desktop.module.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/file-explorer/file-explorer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".button {\n  cursor: pointer;\n  margin-top: 0.5rem;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-top: 0.5rem;\n  padding-bottom: 0.5rem;\n  margin-bottom: 0.5rem;\n  display: inline-block;\n  color: black;\n  border-radius: 0;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/desktop/file-explorer/file-explorer.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"files\">\n  <h2 class=\"normal\">\n    <i class=\"fa fa-folder-open-o\" aria-hidden=\"true\"></i>\n    <ng-container *ngFor=\"let folder of chain; let last=last\">\n      <span *ngIf=\"last\" class=\"bg-washed-blue\">{{folder.name}}</span>\n      <span *ngIf=\"!last\" class=\"underline pointer\" (click)=\"goUpTo(folder)\">{{folder.name}}</span>\n      <i *ngIf=\"!last\" class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\n    </ng-container>\n  </h2>\n  <div>\n    <div *ngIf=\"!addNew\">\n      <a role=\"button\" class=\"f6 link pointer ba mt2 ph3 pv2 ml2 mb2 dib black\" (click)=\"toggleAddFile()\">\n        <i class=\"fa fa-plus\" aria-hidden=\"true\"></i> New\n      </a>\n    </div>\n    <div *ngIf=\"addNew\" class=\"ba\">\n      <div>\n        <a class=\"ml2 f6 link pointer ba mt2 ph3 pv2 mb2 dib black\" (click)=\"toggleAddFile()\">\n          <i class=\"fa fa-times\" aria-hidden=\"true\"></i> Cancel\n        </a>\n        <div class=\"f6 ml2\">\n          <small class=\"black-80\">\n            This will create a new item at the current location,\n            <span class=\"bg-washed-blue\">{{chain[chain.length - 1].name}}</span>.\n            <br>You can still change folders to change this location.\n          </small>\n        </div>\n\n        <div class=\"ml2 w-75\">\n          <form (ngSubmit)=\"onSubmit()\" #heroForm=\"ngForm\">\n            <div class=\"pb2\">\n              <label class=\"f6 b db mb2\" for=\"name\">Name</label>\n              <input type=\"text\" class=\"input-reset ba b--black-20 pa2 mb2 db w-75\" id=\"name\" required [(ngModel)]=\"model.name\" name=\"name\"\n                #name=\"ngModel\">\n              <div [hidden]=\"name.valid || name.pristine\" class=\"light-red\">\n                Name is required\n              </div>\n            </div>\n\n            <div class=\"pb2\">\n              <label class=\"f6 b db mb2\" for=\"file-type\">Item Type</label>\n              <select class=\"ba bg-white b--black-20 pa2 mb2 h2 db w-75\" id=\"file-type\" required [(ngModel)]=\"model.type\" name=\"file-type\"\n                #type=\"ngModel\">\n                <option *ngFor=\"let pow of formFileType\" [value]=\"pow.type\">{{pow.name}}</option>\n              </select>\n            </div>\n            <button type=\"submit\" class=\"button\" *ngIf=\"heroForm.form.valid\">Create</button>\n            <button type=\"submit\" [disabled]=\"true\" class=\"button black-50 bg-washed-red\" *ngIf=\"!heroForm.form.valid\">Create</button>\n            <!-- <button class=\"button\" [disabled]=\"!heroForm.form.valid\">Create</button> -->\n          </form>\n        </div>\n        <!-- <csc428-new-file-form></csc428-new-file-form> -->\n      </div>\n    </div>\n\n  </div>\n  <table class=\"w-100\">\n    <thead>\n      <tr class=\"tl\">\n        <th></th>\n        <th>name</th>\n        <th>type</th>\n        <th>last modified</th>\n      </tr>\n    </thead>\n    <tbody>\n      <ng-container *ngFor=\"let file of files; let idx=index\">\n        <!-- FOLDER -->\n        <tr *ngIf=\"file.type == fileType.FOLDER\" class=\"hover-bg-light-gray w-100 bb b--gray pointer pv2\">\n          <td (click)=\"goDeeper(file)\">\n            <i class=\"fa fa-folder-o\" aria-hidden=\"true\"></i>\n          </td>\n          <td *ngIf=\"idx === editRowId\">\n            <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n            <small>press enter to submit</small>\n          </td>\n          <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n          <td (click)=\"goDeeper(file)\">folder</td>\n          <td (click)=\"goDeeper(file)\">{{file.updated.toLocaleString()}}</td>\n        </tr>\n        <!-- VIDEO -->\n        <tr class=\"not-allowed\" *ngIf=\"file.type == fileType.MOVIE\">\n          <td>\n            <i class=\"fa fa-file-movie-o\" aria-hidden=\"true\"></i>\n          </td>\n          <td *ngIf=\"idx === editRowId\">\n            <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n            <small>press enter to submit</small>\n          </td>\n          <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n          <td (click)=\"openFile(file)\">video</td>\n          <td (click)=\"openFile(file)\">{{file.updated.toLocaleString()}}</td>\n        </tr>\n        <!-- PDF -->\n        <tr *ngIf=\"file.type == fileType.PDF\" class=\"hover-bg-light-gray w-100 bb b--gray pointer pv2\">\n          <td (click)=openFile(file)>\n            <i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i>\n          </td>\n          <td *ngIf=\"idx === editRowId\">\n            <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n            <small>press enter to submit</small>\n          </td>\n          <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n          <td (click)=\"openFile(file)\">pdf</td>\n          <td (click)=\"openFile(file)\">{{file.updated.toLocaleString()}}</td>\n        </tr>\n        <!-- TEXT -->\n        <tr *ngIf=\"file.type == fileType.TEXT\" class=\"hover-bg-light-gray w-100 bb b--gray pointer pv2\">\n          <td (click)=openFile(file)>\n            <i class=\"fa fa-file-text-o\" aria-hidden=\"true\"></i>\n          </td>\n          <td *ngIf=\"idx === editRowId\">\n            <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n            <small>press enter to submit</small>\n          </td>\n          <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n          <td (click)=\"openFile(file)\">text</td>\n          <td (click)=\"openFile(file)\">{{file.updated.toLocaleString()}}</td>\n        </tr>\n      </ng-container>\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "../../../../../src/app/desktop/file-explorer/file-explorer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileExplorerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_files_service__ = __webpack_require__("../../../../../src/app/desktop/services/files.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_pdf_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/pdf-viewer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_video_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/video-viewer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_text_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/text-viewer.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FileExplorerComponent = (function () {
    function FileExplorerComponent(zone, filesService, movieViewer, pdfViewer, textViewer) {
        var _this = this;
        this.zone = zone;
        this.filesService = filesService;
        this.movieViewer = movieViewer;
        this.pdfViewer = pdfViewer;
        this.textViewer = textViewer;
        this.chain = [];
        this.fileType = __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */];
        this.editRowId = -1;
        this.addNew = false;
        this.title = 'File Explorer';
        this.files = [];
        this.formFileType = [
            {
                name: 'folder',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER
            },
            {
                name: 'pdf',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].PDF
            },
            {
                name: 'text',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].TEXT
            },
            {
                name: 'video',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].MOVIE
            }
        ];
        this.model = {
            id: -2,
            name: '',
            type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER,
            updated: new Date()
        };
        this.submitted = false;
        this.filesService.currentFiles.subscribe(function (fs) {
            return _this.zone.run(function () { return (_this.fileTree = fs); });
        });
    }
    FileExplorerComponent.prototype.ngOnInit = function () {
        this.getRoot();
        this.goDeeper(this.files[0]);
    };
    FileExplorerComponent.prototype.onSubmit = function () {
        console.log(this.fileTree);
        this.filesService.addFile(this.chain[this.chain.length - 1], this.model);
        // reset the views
        // .subscribe(fs => (this.fileTree = fs));
        var currentFolder = this.chain[this.chain.length - 1];
        this.files = this.filesService.getChildrenOfFolder(currentFolder, this.fileTree);
        this.model = {
            id: -2,
            name: '',
            type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER,
            updated: new Date()
        };
        this.addNew = false;
    };
    // when clicking away from an object, save :)
    FileExplorerComponent.prototype.ngAfterViewChecked = function () {
        if (this.selectedInput) {
            this.selectedInput.nativeElement.focus();
        }
    };
    FileExplorerComponent.prototype.getRoot = function () {
        this.files = this.filesService.getFoldersAtDepth(0);
    };
    FileExplorerComponent.prototype.edit = function (idx, file) {
        this.editingFile = file;
        this.editRowId = idx;
    };
    FileExplorerComponent.prototype.noEdit = function () {
        this.filesService.update(this.editingFile);
        this.editRowId = -1;
    };
    FileExplorerComponent.prototype.goUpTo = function (toFolder) {
        var indexOfToFolder = this.chain.indexOf(toFolder);
        this.chain = this.chain.slice(0, indexOfToFolder + 1);
        this.files = this.filesService.getChildrenOfFolder(toFolder, this.fileTree);
    };
    FileExplorerComponent.prototype.goDeeper = function (fromFolder) {
        this.chain.push(fromFolder);
        this.files = this.filesService.getChildrenOfFolder(fromFolder);
    };
    FileExplorerComponent.prototype.toggleAddFile = function () {
        this.addNew = !this.addNew;
    };
    FileExplorerComponent.prototype.openFile = function (file) {
        switch (file.type) {
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER:
                this.goDeeper(file);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].MOVIE:
                this.movieViewer.open(file);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].PDF:
                console.log('opening pdf');
                this.pdfViewer.open(file);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].TEXT:
                this.textViewer.open(file);
                break;
            default:
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('selectedInput'),
        __metadata("design:type", Object)
    ], FileExplorerComponent.prototype, "selectedInput", void 0);
    FileExplorerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-file-explorer',
            template: __webpack_require__("../../../../../src/app/desktop/file-explorer/file-explorer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/desktop/file-explorer/file-explorer.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_files_service__["b" /* FilesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_files_service__["b" /* FilesService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_video_viewer_service__["a" /* VideoViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_video_viewer_service__["a" /* VideoViewerService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_pdf_viewer_service__["a" /* PdfViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_pdf_viewer_service__["a" /* PdfViewerService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_text_viewer_service__["a" /* TextViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_text_viewer_service__["a" /* TextViewerService */]) === "function" && _e || Object])
    ], FileExplorerComponent);
    return FileExplorerComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=file-explorer.component.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/file-explorer/new-file.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"ml2 w-75\">\n    <form (ngSubmit)=\"onSubmit()\" #heroForm=\"ngForm\">\n        <div class=\"pb2\">\n            <label class=\"f6 b db mb2\" for=\"name\">Name</label>\n            <input type=\"text\" class=\"input-reset ba b--black-20 pa2 mb2 db w-75\" id=\"name\" required [(ngModel)]=\"model.name\" name=\"name\"\n                #name=\"ngModel\">\n            <div [hidden]=\"name.valid || name.pristine\" class=\"light-red\">\n                Name is required\n            </div>\n        </div>\n\n        <div class=\"pb2\">\n            <label class=\"f6 b db mb2\" for=\"file-type\">Item Type</label>\n            <select class=\"ba bg-white b--black-20 pa2 mb2 h2 db w-75\" id=\"file-type\" required [(ngModel)]=\"model.type\" name=\"file-type\"\n                #type=\"ngModel\">\n                <option *ngFor=\"let pow of fileType\" [value]=\"pow.type\">{{pow.name}}</option>\n            </select>\n        </div>\n\n        <button type=\"submit\" class=\"button\" [disabled]=\"!heroForm.form.valid\">Create</button>\n    </form>\n</div>"

/***/ }),

/***/ "../../../../../src/app/desktop/file-explorer/new-file.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewFileFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services__ = __webpack_require__("../../../../../src/app/desktop/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var NewFileFormComponent = (function () {
    function NewFileFormComponent() {
        this.fileType = [
            {
                name: 'folder',
                type: __WEBPACK_IMPORTED_MODULE_1__services__["b" /* FileType */].FOLDER
            },
            {
                name: 'pdf',
                type: __WEBPACK_IMPORTED_MODULE_1__services__["b" /* FileType */].PDF
            },
            {
                name: 'text',
                type: __WEBPACK_IMPORTED_MODULE_1__services__["b" /* FileType */].TEXT
            },
            {
                name: 'video',
                type: __WEBPACK_IMPORTED_MODULE_1__services__["b" /* FileType */].MOVIE
            }
        ];
        this.model = {
            id: -2,
            name: '',
            type: __WEBPACK_IMPORTED_MODULE_1__services__["b" /* FileType */].FOLDER,
            updated: new Date()
        };
        this.submitted = false;
    }
    NewFileFormComponent.prototype.onSubmit = function () {
        this.submitted = true;
    };
    NewFileFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-new-file-form',
            template: __webpack_require__("../../../../../src/app/desktop/file-explorer/new-file.component.html"),
            styles: [
                "\n"
            ]
        })
    ], NewFileFormComponent);
    return NewFileFormComponent;
}());

//# sourceMappingURL=new-file.component.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/file-selector/file-selector.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".fs-container {\n  position: fixed; /* Stay in place */\n  z-index: 1; /* Sit on top */\n  left: 0;\n  top: 0;\n  width: 100%; /* Full width */\n  height: 100%; /* Full height */\n  overflow: auto; /* Enable scroll if needed */\n  background-color: rgb(0, 0, 0); /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.1); /* Black w/ opacity */\n}\n\n.fs-close {\n  cursor: pointer;\n  float: right;\n  font-size: 28px;\n  font-weight: bold;\n}\n\n.fs-content {\n  box-shadow: 2px 2px 2px black;\n  background-color: #faebd7;\n  margin: auto; /* 15% from the top and centered */\n  padding: 20px;\n  border: 6px solid gray;\n  height: 80vh;\n  width: 80%; /* Could be more or less, depending on screen size */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/desktop/file-selector/file-selector.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"hide\" class=\"dn\">\n</div>\n<div *ngIf=\"!hide\">\n  <div class=\"fs-container\">\n    <div class=\"fs-content\">\n      <div (click)=\"close()\" class=\"fs-close\">X</div>\n      <div *ngIf=\"files\">\n        <h2 class=\"normal\">\n          <i class=\"fa fa-folder-open-o\" aria-hidden=\"true\"></i>\n          <ng-container *ngFor=\"let folder of chain; let last=last\">\n            <span *ngIf=\"last\" class=\"bg-washed-blue\">{{folder.name}}</span>\n            <span *ngIf=\"!last\" class=\"underline pointer\" (click)=\"goUpTo(folder)\">{{folder.name}}</span>\n            <i *ngIf=\"!last\" class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\n          </ng-container>\n        </h2>\n        <table class=\"w-100\">\n          <thead>\n            <tr class=\"tl\">\n              <th></th>\n              <th>name</th>\n              <th>type</th>\n              <th>last modified</th>\n            </tr>\n          </thead>\n          <tbody>\n            <ng-container *ngFor=\"let file of files; let idx=index\">\n              <!-- FOLDER -->\n              <tr *ngIf=\"file.type == fileType.FOLDER\" class=\"hover-bg-light-gray w-100 bb b--gray pointer pv2\">\n                <td (click)=\"goDeeper(file)\">\n                  <i class=\"fa fa-folder-o\" aria-hidden=\"true\"></i>\n                </td>\n                <td *ngIf=\"idx === editRowId\">\n                  <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n                  <small>press enter to submit</small>\n                </td>\n                <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n                <td (click)=\"goDeeper(file)\">folder</td>\n                <td (click)=\"goDeeper(file)\">{{file.updated.toLocaleString()}}</td>\n              </tr>\n              <!-- VIDEO -->\n              <tr class=\"not-allowed\" *ngIf=\"file.type == fileType.MOVIE\">\n                <td>\n                  <i class=\"fa fa-file-movie-o\" aria-hidden=\"true\"></i>\n                </td>\n                <td *ngIf=\"idx === editRowId\">\n                  <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n                  <small>press enter to submit</small>\n                </td>\n                <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n                <td (click)=\"openFile(file)\">video</td>\n                <td (click)=\"openFile(file)\">{{file.updated.toLocaleString()}}</td>\n              </tr>\n              <!-- PDF -->\n              <tr *ngIf=\"file.type == fileType.PDF\" class=\"hover-bg-light-gray bg-light-blue w-100 bb b--gray pointer pv2\">\n                <td (click)=\"addFile(file)\">\n                  <i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i>\n                </td>\n                <td *ngIf=\"idx === editRowId\">\n                  <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n                  <small>press enter to submit</small>\n                </td>\n                <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n                <td (click)=\"addFile(file)\">pdf</td>\n                <td (click)=\"addFile(file)\">{{file.updated.toLocaleString()}}</td>\n              </tr>\n              <!-- TEXT -->\n              <tr *ngIf=\"file.type == fileType.TEXT\" class=\"hover-bg-light-gray w-100 bb b--gray gray pv2\">\n                <td (click)=openFile(file)>\n                  <i class=\"fa fa-file-text-o\" aria-hidden=\"true\"></i>\n                </td>\n                <td *ngIf=\"idx === editRowId\">\n                  <input #selectedInput type=\"text\" [(ngModel)]=\"file.name\" (blur)=\"noEdit()\" (keyup.enter)=\"noEdit()\" />\n                  <small>press enter to submit</small>\n                </td>\n                <td *ngIf=\"idx !== editRowId\" style=\"cursor: cell\" (click)=\"edit(idx, file)\">{{file.name}}</td>\n                <td (click)=\"openFile(file)\">text</td>\n                <td (click)=\"openFile(file)\">{{file.updated.toLocaleString()}}</td>\n              </tr>\n            </ng-container>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/desktop/file-selector/file-selector.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileSelectorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_files_service__ = __webpack_require__("../../../../../src/app/desktop/services/files.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_pdf_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/pdf-viewer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_video_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/video-viewer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_text_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/text-viewer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_file_selector_service__ = __webpack_require__("../../../../../src/app/desktop/services/file-selector.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FileSelectorComponent = (function () {
    function FileSelectorComponent(zone, filesService, movieViewer, pdfViewer, textViewer, fileSelectorService) {
        var _this = this;
        this.zone = zone;
        this.filesService = filesService;
        this.movieViewer = movieViewer;
        this.pdfViewer = pdfViewer;
        this.textViewer = textViewer;
        this.fileSelectorService = fileSelectorService;
        this.hide = true;
        this.chain = [];
        this.fileType = __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */];
        this.editRowId = -1;
        this.addNew = false;
        this.title = 'File Explorer';
        this.files = [];
        this.formFileType = [
            {
                name: 'folder',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER
            },
            {
                name: 'pdf',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].PDF
            },
            {
                name: 'text',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].TEXT
            },
            {
                name: 'video',
                type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].MOVIE
            }
        ];
        this.selectedFiles = [];
        this.model = {
            id: -2,
            name: '',
            type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER,
            updated: new Date()
        };
        this.submitted = false;
        this.filesService.currentFiles.subscribe(function (fs) {
            return _this.zone.run(function () { return (_this.fileTree = fs); });
        });
        this.fileSelectorService.currentHideStatus.subscribe(function (h) { return (_this.hide = h); });
    }
    FileSelectorComponent.prototype.close = function () {
        this.fileSelectorService.toggle();
        this.fileSelectorService.fileFinish();
    };
    FileSelectorComponent.prototype.ngOnInit = function () {
        this.fileSelectorService.fileStart();
        this.getRoot();
        this.goDeeper(this.files[0]);
    };
    FileSelectorComponent.prototype.onSubmit = function () {
        console.log(this.fileTree);
        this.filesService.addFile(this.chain[this.chain.length - 1], this.model);
        // reset the views
        // .subscribe(fs => (this.fileTree = fs));
        var currentFolder = this.chain[this.chain.length - 1];
        this.files = this.filesService.getChildrenOfFolder(currentFolder, this.fileTree);
        this.model = {
            id: -2,
            name: '',
            type: __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER,
            updated: new Date()
        };
        this.addNew = false;
    };
    // when clicking away from an object, save :)
    // ngAfterViewChecked() {
    //   if (this.selectedInput) {
    //     this.selectedInput.nativeElement.focus();
    //   }
    // }
    FileSelectorComponent.prototype.addFile = function (f) {
        if (!this.selectedFiles.find(function (file) { return file.id === f.id; })) {
            this.selectedFiles.push(f);
            this.fileSelectorService.addFile(f);
        }
    };
    FileSelectorComponent.prototype.getRoot = function () {
        this.files = this.filesService.getFoldersAtDepth(0);
    };
    FileSelectorComponent.prototype.edit = function (idx, file) {
        this.editingFile = file;
        this.editRowId = idx;
    };
    FileSelectorComponent.prototype.noEdit = function () {
        this.filesService.update(this.editingFile);
        this.editRowId = -1;
    };
    FileSelectorComponent.prototype.goUpTo = function (toFolder) {
        var indexOfToFolder = this.chain.indexOf(toFolder);
        this.chain = this.chain.slice(0, indexOfToFolder + 1);
        this.files = this.filesService.getChildrenOfFolder(toFolder, this.fileTree);
    };
    FileSelectorComponent.prototype.goDeeper = function (fromFolder) {
        this.chain.push(fromFolder);
        this.files = this.filesService.getChildrenOfFolder(fromFolder);
    };
    FileSelectorComponent.prototype.toggleAddFile = function () {
        this.addNew = !this.addNew;
    };
    FileSelectorComponent.prototype.openFile = function (file) {
        switch (file.type) {
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].FOLDER:
                this.goDeeper(file);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].MOVIE:
                this.movieViewer.open(file);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].PDF:
                console.log('opening pdf');
                this.pdfViewer.open(file);
                break;
            case __WEBPACK_IMPORTED_MODULE_1__services_files_service__["a" /* FileType */].TEXT:
                this.textViewer.open(file);
                break;
            default:
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('selectedInput'),
        __metadata("design:type", Object)
    ], FileSelectorComponent.prototype, "selectedInput", void 0);
    FileSelectorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-file-selector',
            template: __webpack_require__("../../../../../src/app/desktop/file-selector/file-selector.component.html"),
            styles: [__webpack_require__("../../../../../src/app/desktop/file-selector/file-selector.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_files_service__["b" /* FilesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_files_service__["b" /* FilesService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_video_viewer_service__["a" /* VideoViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_video_viewer_service__["a" /* VideoViewerService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services_pdf_viewer_service__["a" /* PdfViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_pdf_viewer_service__["a" /* PdfViewerService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__services_text_viewer_service__["a" /* TextViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_text_viewer_service__["a" /* TextViewerService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__services_file_selector_service__["a" /* FileSelectorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__services_file_selector_service__["a" /* FileSelectorService */]) === "function" && _f || Object])
    ], FileSelectorComponent);
    return FileSelectorComponent;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=file-selector.component.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/pdf-viewer/pdf-viewer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/desktop/pdf-viewer/pdf-viewer.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"currentPDF\">\n  <div class=\"center measure-wide f5 lh-copy ph2 athelas\">\n    <h1 (click)=\"copyTitle()\" class=\"cursor-cell dim f1 lh-title\" id=\"title\">{{currentPDF.data.title}}</h1>\n    <div class=\"lh-copy\">\n      <ng-container *ngFor=\"let words of tokenize(currentPDF.data.body)\">\n        <ng-container *ngIf=\"words.id == -1\">\n          <br>\n        </ng-container>\n        <ng-container *ngIf=\"words.id !== -1\">\n          <span (click)=\"copy( words)\" class=\"dim cursor-cell\" id=\"select-{{words.id}}\">{{words.val}}</span>\n        </ng-container>\n      </ng-container>\n    </div>\n  </div>\n</div>\n<div *ngIf=\"!currentPDF\">\n  Select a PDF\n</div>"

/***/ }),

/***/ "../../../../../src/app/desktop/pdf-viewer/pdf-viewer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PdfViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/desktop/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var PdfViewerComponent = (function () {
    function PdfViewerComponent(vcr, sequitorService, pdfViewerService, dom) {
        this.sequitorService = sequitorService;
        this.pdfViewerService = pdfViewerService;
        this.dom = dom;
    }
    PdfViewerComponent.prototype.copyToClipboard = function (text) {
        // from: https://www.bennadel.com/blog/3235-creating-a-simple-copy-to-clipboard-directive-in-angular-2-4-9.htm
        var textarea;
        try {
            // In order to execute the "Copy" command, we actually have to have
            // a "selection" in the currently rendered document. As such, we're
            // going to inject a Textarea element and .select() it in order to
            // force a selection.
            // --
            // NOTE: This Textarea is being rendered off-screen.
            textarea = this.dom.createElement('textarea');
            textarea.style.height = '0px';
            textarea.style.left = '-100px';
            textarea.style.opacity = '0';
            textarea.style.position = 'fixed';
            textarea.style.top = '-100px';
            textarea.style.width = '0px';
            this.dom.body.appendChild(textarea);
            // Set and select the value (creating an active Selection range).
            textarea.value = text;
            textarea.select();
            // Ask the browser to copy the current selection to the clipboard.
            this.dom.execCommand('copy');
        }
        finally {
            // Cleanup - remove the Textarea from the DOM if it was injected.
            if (textarea && textarea.parentNode) {
                textarea.parentNode.removeChild(textarea);
            }
        }
    };
    PdfViewerComponent.prototype.copy = function (val) {
        // const copyText = document.getElementById(`${val.id}`);
        // document.execCommand('Copy');
        var c = new __WEBPACK_IMPORTED_MODULE_2__services__["e" /* SequitorCopy */](val.val, "#" + val.id, this.currentPDF);
        this.sequitorService.addInput(c);
        this.copyToClipboard(val.val);
    };
    PdfViewerComponent.prototype.copyTitle = function () {
        var c = new __WEBPACK_IMPORTED_MODULE_2__services__["e" /* SequitorCopy */](this.currentPDF.data.title, "title", this.currentPDF);
        this.sequitorService.addInput(c);
        this.copyToClipboard(this.currentPDF.data.title);
    };
    PdfViewerComponent.prototype.tokenize = function (text) {
        if (text === undefined)
            return [];
        var texts = text.split('\n');
        return texts
            .map(function (t) {
            return t.split(' ').reduce(function (strs, str, i) {
                if (str === '\n') {
                    strs.push({
                        id: -1,
                        val: ''
                    });
                    return strs;
                }
                var lastIdx = strs.length - 1;
                var lastEntry = strs[lastIdx];
                var lastVal = lastEntry.val;
                if (lastVal !== undefined && lastVal.split(' ').length < 2) {
                    lastEntry.val = lastVal + " " + str;
                    lastEntry.id = i;
                    strs[lastIdx] = lastEntry;
                    return strs;
                }
                strs.push({
                    id: i,
                    val: str
                });
                return strs;
            }, [{ id: -1, val: '' }]);
        })
            .reduce(function (acc, strs) { return acc.concat(strs, [{ id: -1, val: '' }]); }, []);
    };
    PdfViewerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pdfViewerService.currentPDF.subscribe(function (pdf) { return (_this.currentPDF = pdf); });
    };
    PdfViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-pdf-viewer',
            template: __webpack_require__("../../../../../src/app/desktop/pdf-viewer/pdf-viewer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/desktop/pdf-viewer/pdf-viewer.component.css")]
        }),
        __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["DOCUMENT"])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services__["f" /* SequitorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["f" /* SequitorService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PdfViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PdfViewerService */]) === "function" && _c || Object, Object])
    ], PdfViewerComponent);
    return PdfViewerComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=pdf-viewer.component.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/_sequitor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return printRuleExpansion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return makeGrammar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return printGrammar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return printHTMLGrammar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Rule; });
/*===================================================**
 * Algorithm From http://www.sequitur.info
 * All credit goes to
 *    Craig Nevill-Manning, Google
 *    Ian Witten, University of Waikato, New Zealand
 *====================================================*/
var uniqueRuleNumber = 1;
var digramIndex = {};
var Rule = (function () {
    function Rule() {
        // the guard node in the linked list of symbols that make up the rule
        // It points forward to the first symbol in the rule, and backwards
        // to the last symbol in the rule. Its own value points to the rule data
        // structure, so that symbols can find out which rule they're in
        this.guard = new SeqSymbol(this);
        this.referenceCount = 0;
        this.number = 0;
        this.guard.join(this.guard);
        this.referenceCount = 0;
        this.number = 0;
        this.uniqueNumber = uniqueRuleNumber++;
    }
    Rule.prototype.first = function () {
        return this.guard.getNext();
    };
    Rule.prototype.last = function () {
        return this.guard.getPrev();
    };
    Rule.prototype.incrementReferenceCount = function () {
        this.referenceCount++;
    };
    Rule.prototype.decrementReferenceCount = function () {
        this.referenceCount--;
    };
    Rule.prototype.getReferenceCount = function () {
        return this.referenceCount;
    };
    Rule.prototype.setNumber = function (i) {
        this.number = i;
    };
    Rule.prototype.getNumber = function () {
        return this.number;
    };
    return Rule;
}());
var SeqSymbol = (function () {
    // initializes a new symbol. If it is non-terminal, increments the reference
    // count of the corresponding rule
    function SeqSymbol(value) {
        this.next = null;
        this.prev = null;
        this.terminal = null;
        this.rule = null;
        if (typeof value === 'string') {
            this.terminal = value;
        }
        else if (typeof value === 'object') {
            if (value.terminal) {
                this.terminal = value.terminal;
            }
            else if (value.rule) {
                this.rule = value.rule;
                this.rule.incrementReferenceCount();
            }
            else {
                this.rule = value;
                this.rule.incrementReferenceCount();
            }
        }
        else {
            console.log('Did not recognize ' + value);
        }
    }
    /**
     * links two symbols together, removing any old digram from the hash table.
     */
    SeqSymbol.prototype.join = function (right) {
        if (this.next) {
            this.deleteDigram();
            // This is to deal with triples, where we only record the second
            // pair of the overlapping digrams. When we delete the second pair,
            // we insert the first pair into the hash table so that we don't
            // forget about it.  e.g. abbbabcbb
            if (right.prev &&
                right.next &&
                right.value() === right.prev.value() &&
                right.value() === right.next.value()) {
                digramIndex[right.hashValue()] = right;
            }
            if (this.prev &&
                this.next &&
                this.value() === this.next.value() &&
                this.value() === this.prev.value()) {
                digramIndex[this.hashValue()] = this;
            }
        }
        this.next = right;
        right.prev = this;
    };
    /**
     * cleans up for symbol deletion: removes hash table entry and decrements
     * rule reference count.
     */
    SeqSymbol.prototype.delete = function () {
        this.prev.join(this.next);
        if (!this.isGuard()) {
            this.deleteDigram();
            if (this.getRule()) {
                this.getRule().decrementReferenceCount();
            }
        }
    };
    /**
     * Removes the digram from the hash table
     */
    SeqSymbol.prototype.deleteDigram = function () {
        if (this.isGuard() || this.next.isGuard()) {
            return;
        }
        if (digramIndex[this.hashValue()] === this) {
            digramIndex[this.hashValue()] = null;
        }
    };
    /**
     * Inserts a symbol after this one.
     */
    SeqSymbol.prototype.insertAfter = function (symbol) {
        symbol.join(this.next);
        this.join(symbol);
    };
    /**
     * Returns true if this is the guard node marking the beginning and end of a
     * rule.
     */
    SeqSymbol.prototype.isGuard = function () {
        return (this.getRule() &&
            this.getRule()
                .first()
                .getPrev() === this);
    };
    /**
     * getRule() returns rule if a symbol is non-terminal, and null otherwise.
     */
    SeqSymbol.prototype.getRule = function () {
        return this.rule;
    };
    SeqSymbol.prototype.getNext = function () {
        return this.next;
    };
    SeqSymbol.prototype.getPrev = function () {
        return this.prev;
    };
    SeqSymbol.prototype.getTerminal = function () {
        return this.terminal;
    };
    /**
     * Checks a new digram. If it appears elsewhere, deals with it by calling
     * match(), otherwise inserts it into the hash table.
     */
    SeqSymbol.prototype.check = function () {
        if (this.isGuard() || this.next.isGuard()) {
            return 0;
        }
        var match = digramIndex[this.hashValue()];
        if (!match) {
            digramIndex[this.hashValue()] = this;
            return false;
        }
        if (match.getNext() !== this) {
            this.processMatch(match);
        }
        return true;
    };
    /**
     * This symbol is the last reference to its rule. It is deleted, and the
     * contents of the rule substituted in its place.
     */
    SeqSymbol.prototype.expand = function () {
        var left = this.getPrev();
        var right = this.getNext();
        var first = this.getRule().first();
        var last = this.getRule().last();
        if (digramIndex[this.hashValue()] === this) {
            digramIndex[this.hashValue()] = null;
        }
        left.join(first);
        last.join(right);
        digramIndex[last.hashValue()] = last;
    };
    /**
     * Replace a digram with a non-terminal
     */
    SeqSymbol.prototype.substitute = function (rule) {
        var prev = this.prev;
        prev.getNext().delete();
        prev.getNext().delete();
        prev.insertAfter(new SeqSymbol(rule));
        if (!prev.check()) {
            prev.next.check();
        }
    };
    /**
     * Deal with a matching digram.
     */
    SeqSymbol.prototype.processMatch = function (match) {
        var rule;
        // reuse an existing rule
        if (match.getPrev().isGuard() &&
            match
                .getNext()
                .getNext()
                .isGuard()) {
            rule = match.getPrev().getRule();
            this.substitute(rule);
        }
        else {
            // create a new rule
            rule = new Rule();
            rule.last().insertAfter(new SeqSymbol(this));
            rule.last().insertAfter(new SeqSymbol(this.getNext()));
            match.substitute(rule);
            this.substitute(rule);
            digramIndex[rule.first().hashValue()] = rule.first();
        }
        // check for an underused rule
        if (rule.first().getRule() &&
            rule
                .first()
                .getRule()
                .getReferenceCount() === 1) {
            rule.first().expand();
        }
    };
    SeqSymbol.prototype.value = function () {
        return this.rule ? this.rule : this.terminal;
    };
    SeqSymbol.prototype.stringValue = function () {
        if (this.getRule()) {
            return 'rule:' + this.rule.uniqueNumber;
        }
        else {
            return this.terminal;
        }
    };
    SeqSymbol.prototype.hashValue = function () {
        return this.stringValue() + '+' + this.next.stringValue();
    };
    return SeqSymbol;
}());
// print the rules out
var ruleSet;
var outputArray;
var lineLength;
/**
 * @param {Rule} rule
 */
function printRule(rule) {
    for (var symbol = rule.first(); !symbol.isGuard(); symbol = symbol.getNext()) {
        if (symbol.getRule()) {
            var ruleNumber;
            if (ruleSet[symbol.getRule().getNumber()] === symbol.getRule()) {
                ruleNumber = symbol.getRule().getNumber();
            }
            else {
                ruleNumber = ruleSet.length;
                symbol.getRule().setNumber(ruleSet.length);
                ruleSet.push(symbol.getRule());
            }
            outputArray.push(ruleNumber + ' ');
            lineLength += (ruleNumber + ' ').length;
        }
        else {
            outputArray.push(printTerminal(symbol.value()));
            outputArray.push(' ');
            lineLength += 2;
        }
    }
}
function printTerminal(value) {
    if (value === ' ') {
        //    return '\u2423'; // open box (typographic blank indicator).
        return '_'; // open box (typographic blank indicator).
    }
    else if (value === '\n') {
        return '&crarr;';
    }
    else if (value === '\t') {
        return '&#8677;';
    }
    else if (value === '\\' ||
        value === '(' ||
        value === ')' ||
        value === '_' ||
        value.match(/[0-9]/)) {
        return '\\' + value;
    }
    else {
        return value;
    }
}
function printRuleExpansion(rule, lst) {
    if (lst === void 0) { lst = []; }
    for (var symbol = rule.first(); !symbol.isGuard(); symbol = symbol.getNext()) {
        if (symbol.getRule()) {
            printRuleExpansion(symbol.getRule(), lst);
        }
        else {
            lst.push(symbol.value());
            outputArray.push(printTerminal(symbol.value()));
        }
    }
    return lst;
}
function printGrammar(S) {
    outputArray = [];
    ruleSet = [];
    ruleSet[0] = S;
    for (var i = 0; ruleSet[i]; i++) {
        outputArray.push(i + ' > ');
        lineLength = (i + '   ').length;
        printRule(ruleSet[i]);
        if (i > 0) {
            for (var j = lineLength; j < 50; j++) {
                outputArray.push(' ');
            }
            var v = [];
            printRuleExpansion(ruleSet[i], v);
        }
        outputArray.push('\n');
    }
    return { str: outputArray.join(''), ruleSet: ruleSet };
}
function printHTMLGrammar(S) {
    outputArray = [];
    ruleSet = [];
    ruleSet[0] = S;
    for (var i = 0; ruleSet[i]; i++) {
        outputArray.push(i + ' &rarr; ');
        lineLength = (i + '   ').length;
        printRule(ruleSet[i]);
        if (i > 0) {
            for (var j = lineLength; j < 50; j++) {
                outputArray.push('&nbsp;');
            }
            printRuleExpansion(ruleSet[i]);
        }
        outputArray.push('<br>\n');
    }
    return outputArray.join('');
}
function makeGrammar(input) {
    digramIndex = {};
    var S = new Rule();
    if (input.length) {
        S.last().insertAfter(new SeqSymbol(input.shift()));
    }
    while (input.length) {
        S.last().insertAfter(new SeqSymbol(input.shift()));
        S.last()
            .getPrev()
            .check();
    }
    return S;
}
// function printHash() {
//   let hash = '';
//   for (let key in digramIndex) {
//     hash += key + ': ' + digramIndex[key] + '\n';
//   }
// }

//# sourceMappingURL=_sequitor.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/debug.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DebugService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DebugService = (function () {
    function DebugService() {
        this.output = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        this.debugValue = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](false);
        this.currentOutput = this.output.asObservable();
        this.currentDebugValue = this.debugValue.asObservable();
    }
    DebugService.prototype.toggle = function () {
        var next = !this.debugValue.getValue();
        if (next === false)
            this.output.next('');
        this.debugValue.next(next);
        return next;
    };
    DebugService.prototype.on = function () {
        this.debugValue.next(true);
    };
    DebugService.prototype.off = function () {
        this.debugValue.next(false);
        this.output.next('');
    };
    DebugService.prototype.write = function (nextVal) {
        this.output.next(nextVal);
    };
    DebugService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], DebugService);
    return DebugService;
}());

//# sourceMappingURL=debug.service.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/file-selector.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileSelectorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__files_service__ = __webpack_require__("../../../../../src/app/desktop/services/files.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FileSelectorService = (function () {
    function FileSelectorService(filesService) {
        this.filesService = filesService;
        this.hide = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"](true);
        this.files = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"]({
            files: [],
            done: false
        });
        this.currentHideStatus = this.hide.asObservable();
        this.currentFileAndStatus = this.files.asObservable();
    }
    FileSelectorService.prototype.toggle = function () {
        var isHidden = this.hide.getValue();
        this.hide.next(!isHidden);
    };
    FileSelectorService.prototype.fileStart = function () {
        var files = this.files.getValue().files;
        this.files.next({
            done: false,
            files: files
        });
    };
    FileSelectorService.prototype.addFile = function (f) {
        var _a = this.files.getValue(), files = _a.files, done = _a.done;
        files.push(f);
        this.files.next({
            done: done,
            files: files
        });
    };
    FileSelectorService.prototype.fileFinish = function () {
        var files = this.files.getValue().files;
        this.files.next({
            done: true,
            files: files
        });
        // this.applyRepetitionService.repeatOn(files);
        console.log('finish', files);
    };
    FileSelectorService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__files_service__["b" /* FilesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__files_service__["b" /* FilesService */]) === "function" && _a || Object])
    ], FileSelectorService);
    return FileSelectorService;
    var _a;
}());

//# sourceMappingURL=file-selector.service.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/files.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FilesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FileType;
(function (FileType) {
    FileType[FileType["FOLDER"] = 0] = "FOLDER";
    FileType[FileType["PDF"] = 1] = "PDF";
    FileType[FileType["MOVIE"] = 2] = "MOVIE";
    FileType[FileType["TEXT"] = 3] = "TEXT";
})(FileType || (FileType = {}));
var FilesService = (function () {
    function FilesService() {
        this.TEXT = [
            {
                value: {
                    id: 15,
                    name: 'todo',
                    updated: new Date(),
                    type: FileType.TEXT,
                    data: {
                        text: "\n        Things To Do (ordered)\n        ------------\n\n        1. Pay Tuition\n        "
                    }
                }
            },
            {
                value: {
                    id: 16,
                    name: 'bear',
                    updated: new Date(),
                    type: FileType.TEXT,
                    data: {
                        text: "\n           ___\n         {~._.~}\n          ( Y )\n         ()~*~()\n         (_)-(_)\n        "
                    }
                }
            }
        ];
        this.MOVIES = {
            value: {
                id: 5,
                name: 'movies',
                updated: new Date(),
                type: FileType.FOLDER
            },
            children: [
                {
                    value: {
                        id: 11,
                        name: 'Halt and Catch Fire',
                        updated: new Date(),
                        type: FileType.FOLDER
                    },
                    children: [
                        {
                            value: {
                                id: 6,
                                name: 's01e01',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        },
                        {
                            value: {
                                id: 7,
                                name: 's01e02',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        },
                        {
                            value: {
                                id: 8,
                                name: 's01e03',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        },
                        {
                            value: {
                                id: 9,
                                name: 's01e04',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        }
                    ]
                },
                {
                    value: {
                        id: 11,
                        name: 'Game of Thrones',
                        updated: new Date(),
                        type: FileType.FOLDER
                    },
                    children: [
                        {
                            value: {
                                id: 6,
                                name: 's08e01',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        },
                        {
                            value: {
                                id: 7,
                                name: 's08e02',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        },
                        {
                            value: {
                                id: 8,
                                name: 's08e03',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        },
                        {
                            value: {
                                id: 9,
                                name: 's08e04',
                                updated: new Date(),
                                type: FileType.MOVIE
                            }
                        }
                    ]
                }
            ]
        };
        this.PDFS = {
            value: { id: 0, name: 'pdfs', updated: new Date(), type: FileType.FOLDER },
            children: [
                {
                    value: {
                        type: FileType.PDF,
                        id: 1,
                        name: 'pdf1',
                        updated: new Date(),
                        data: {
                            title: 'title 1',
                            body: "\nThought leadership theory of change or low-hanging fruit society a\nlow-hanging fruit shared unit of analysis social impact. Paradigm;\npeaceful collaborative consumption strategize thought leader.\nMobilize, vibrant game-changer technology efficient, optimism,\nco-create think tank preliminary thinking grit systems thinking\nstrategize problem-solvers overcome injustice. Resilient scalable,\nhumanitarian storytelling empathetic; compelling optimism,\ninspirational.\n\nAgile strategy granular, best practices inspire sustainable boots\non the ground, her body her rights social intrapreneurship youth\nimpact investing optimism parse greenwashing. Thought leader design\nthinking; parse citizen-centered dynamic; inspirational; youth\nideate because. Corporate social responsibility ideate sustainable\ncultivate inspire change-makers. Empathetic save the world empower\nscale and impact program areas disrupt rubric vibrant social return\non investment correlation. Strategize mass incarceration; global\npolicymaker grit triple bottom line social intrapreneurship\ncompassion. Commitment program area, bandwidth, innovation\neffective altruism game-changer grit policymaker. Empower; our work\nempower communities effective issue outcomes agile equal\nopportunity boots on the ground inspiring revolutionary think tank.\n\nCollaborative cities granular challenges and opportunities save the\nworld because outcomes communities. Improve the world, shine, grit\ncatalyze granular deep dive vibrant. Grit youth, bandwidth,\ncontextualize; data peaceful, resist collective impact low-hanging\nfruit.\n           "
                        }
                    }
                },
                {
                    value: {
                        type: FileType.PDF,
                        id: 2,
                        name: 'pdf2',
                        updated: new Date(),
                        data: {
                            title: 'title 2',
                            body: "\nLeverage program areas collaborative consumption our work social\nintrapreneurship; social innovation targeted save the world her\nbody her rights scale and impact resilient progress radical issue\noutcomes. Design thinking thought leader improve the world\nsustainable uplift rubric. Thought leader society inspirational\nsocial return on investment low-hanging fruit compassion, silo\nfairness vibrant synergy initiative innovate. Contextualize justice\na her body her rights mass incarceration.\n\nData corporate social responsibility, natural resources collective impact\nshine think tank. Do-gooder big data thought provoking, strategize move the\nneedle accessibility, social innovation overcome injustice white paper triple\nbottom line co-create mobilize program areas. To co-create, shared unit of\nanalysis; big data collective impact living a fully ethical life when.\nOptimism strategy leverage inspirational and, segmentation inspiring social\nenterprise effective white paper venture philanthropy thought partnership.\nCompelling natural resources invest cultivate unprecedented challenge,\ncultivate, social intrapreneurship, shared unit of analysis external partners\nenergize granular relief. Expose the truth program areas change-makers, agile\nhumanitarian thought leadership social entrepreneur. Think tank activate;\ncontextualize; human-centered do-gooder.\n\nOvercome injustice equal opportunity, overcome injustice ideate families\neffective preliminary thinking; collaborative cities and dynamic ecosystem\nB-corp segmentation. Move the needle innovation sustainable social impact\ngreenwashing. Agile; justice society bandwidth; do-gooder targeted program\narea emerging transparent. "
                        }
                    }
                },
                {
                    value: {
                        type: FileType.PDF,
                        id: 3,
                        name: 'pdf3',
                        updated: new Date(),
                        data: {
                            title: 'title 3',
                            body: "\nChangemaker, systems thinking; equal opportunity social impact\nshared unit of analysis do-gooder her body her rights.\nRevolutionary, targeted, silo activate, innovation. Efficient the\nresistance, think tank changemaker emerging empathetic best\npractices optimism replicable. Living a fully ethical life\nefficient equal opportunity correlation synergy problem-solvers\ntargeted expose the truth grit program area targeted a.\n\nSocial intrapreneurship empathetic co-creation shine inclusion;\nwhite paper greenwashing, milestones indicators, program area\noptimism ecosystem contextualize expose the truth. Academic\ntechnology, technology commitment capacity building white paper\nhuman-centered best practices. Synergy living a fully ethical life\ncollaborative consumption social innovation movements activate\nideate strategize corporate social responsibility big data\nprogress. Humanitarian improve the world accessibility innovate\ncapacity building design thinking. Rubric, change-makers\ninspirational; social entrepreneurship rubric grit, social\ninnovation initiative, radical segmentation social enterprise\ntechnology.\n\nThought leadership peaceful disrupt policymaker; synergy;\ncollaborate social entrepreneur innovation inclusive entrepreneur\nsocial capital commitment. Energize design thinking catalyze\ninclusion; scalable emerging targeted collaborative cities shared\nvalue collaborative cities overcome injustice agile ecosystem.\nSegmentation commitment and strengthening infrastructure;\npolicymaker catalyze. Black lives matter grit effective; the\ntransparent youth innovation issue outcomes when theory of change\nentrepreneur.\n          "
                        }
                    }
                },
                {
                    value: {
                        type: FileType.PDF,
                        id: 4,
                        name: 'cat',
                        updated: new Date(),
                        data: {
                            title: " CAT(1)",
                            body: "\n\nCAT(1)                    BSD General Commands Manual                   CAT(1) \n\n\nNAME\n     cat -- concatenate and print files\n\nSYNOPSIS\n     cat [-benstuv] [file ...]\n\nDESCRIPTION\n     The cat utility reads files sequentially, writing them to\n     the standard output. The file operands are processed in\n     command-line order. If file is a single dash ('-') or\n     absent, cat reads from the standard input. If file is a UNIX\n     domain socket, cat connects to it and then reads it until\n     EOF. This complements the UNIX domain binding capability\n     available in inetd(8).\n\n     The options are as follows:\n\n     -b      Number the non-blank output lines, starting at 1.\n\n     -e      Display non-printing characters (see the -v option), and\n             display a dollar sign ('$') at the end of each line.\n\n     -n      Number the output lines, starting at 1.\n\n     -s      Squeeze multiple adjacent empty lines, causing the output to be single spaced.\n\n     -t      Display non-printing characters (see the -v option), and display tab characters as '^I'.\n\n     -u      Disable output buffering.\n\n     -v      Display non-printing characters so they are visible.\n             Control characters print as '^X' for control-X; the delete\n             character (octal 0177) prints as '^?'. Non-ASCII characters\n             (with the high bit set) are printed as 'M-' (for meta)\n             followed by the character for the low 7 bits.\n\nEXIT STATUS\n     The cat utility exits 0 on success, and >0 if an error occurs.\n"
                        }
                    }
                }
            ]
        };
        this.files = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({
            value: { id: -1, name: 'home', updated: new Date(), type: FileType.FOLDER },
            children: [this.MOVIES, this.PDFS].concat(this.TEXT)
        });
        this.currentFiles = this.files.asObservable();
    }
    FilesService.prototype.getFoldersAtDepth = function (depth, files) {
        if (files === void 0) { files = this.files.getValue(); }
        return this._getFoldersAtDepth(files, depth);
    };
    FilesService.prototype.getChildrenOfFolder = function (folder, files) {
        if (files === void 0) { files = this.files.getValue(); }
        var found = function (f) { return f.id === folder.id; };
        var tree = this.find_folder(files, found);
        try {
            return tree[0].children.map(function (t) { return t.value; });
        }
        catch (error) {
            return [];
        }
    };
    FilesService.prototype.getParentFolder = function (ofChild, files) {
        if (files === void 0) { files = this.files.getValue(); }
        return this.find_parent_folder(files, ofChild).children.map(function (c) { return c.value; });
    };
    Object.defineProperty(FilesService.prototype, "id", {
        get: function () {
            var id = 17;
            return function () {
                id = id + 1;
                return id;
            };
        },
        enumerable: true,
        configurable: true
    });
    // functions to add files
    FilesService.prototype.addFile = function (parent, child, files) {
        if (files === void 0) { files = this.files.getValue(); }
        var changed = false;
        var self = this;
        function _bfs(node) {
            var foundParent = node.value.id === parent.id;
            var hasChildren = node.children !== undefined;
            if (foundParent && hasChildren) {
                var newChild = Object.assign({}, child);
                newChild.id = self.id();
                var tree = { value: newChild };
                switch (child.type) {
                    case FileType.FOLDER:
                        tree.children = [];
                        break;
                    case FileType.PDF:
                        tree.value.data = { title: '', body: '' };
                        break;
                    case FileType.MOVIE:
                        tree.value.data = {};
                        break;
                    case FileType.TEXT:
                        tree.value.data = { text: '' };
                        break;
                }
                node.children.push(tree);
                changed = true;
            }
            else if (hasChildren) {
                node.children.forEach(function (c) { return _bfs(c); });
            }
        }
        _bfs(files);
        this.files.next(Object.assign({}, files));
        return changed;
    };
    FilesService.prototype.update = function (file, files) {
        if (files === void 0) { files = this.files.getValue(); }
        var changed = false;
        function _bfs(node) {
            if (node.value.id === file.id) {
                node.value = file;
                node.value.updated = new Date();
                changed = true;
            }
            if (node.children)
                node.children.forEach(function (child) { return _bfs(child); });
        }
        _bfs(files);
        this.files.next(files);
        return changed;
    };
    FilesService.prototype.find_files = function (files, found) {
        var foundFiles = [];
        function _bfs(node) {
            if (found(node.value))
                foundFiles.push(node.value);
            if (node.children)
                node.children.forEach(function (child) { return _bfs(child); });
        }
        _bfs(files);
        return foundFiles;
    };
    FilesService.prototype._getFoldersAtDepth = function (files, depth) {
        var foundFolders = [];
        function _bfs(node, _depth) {
            if (_depth === void 0) { _depth = 0; }
            if (_depth === depth)
                foundFolders.push(node.value);
            if (node.children)
                node.children.forEach(function (child) { return _bfs(child, _depth + 1); });
        }
        _bfs(files);
        return foundFolders;
    };
    FilesService.prototype.find_folder = function (files, found) {
        var foundFolders = [];
        function _bfs(node) {
            if (found(node.value) && node.value.type === FileType.FOLDER)
                foundFolders.push(node);
            if (node.children !== undefined)
                node.children.forEach(function (child) { return _bfs(child); });
        }
        _bfs(files);
        return foundFolders;
    };
    FilesService.prototype.find_parent_folder = function (files, ofChild) {
        function _bfs(node) {
            if (node.children !== undefined &&
                node.children.find(function (fileTree) { return fileTree.value.id === ofChild.id; }))
                return node;
            if (node.children !== undefined)
                node.children.forEach(function (child) { return _bfs(child); });
        }
        return _bfs(files);
    };
    /**
     * Returns the first file found with the given name (breadth first)
     * @param name the name of the file
     */
    FilesService.prototype.findFileByName = function (files, name) {
        var _files = this.find_files(files, function (f) { return f.name === name; });
        if (_files.length > 0)
            return files[0];
        return undefined;
    };
    FilesService.prototype.findFolderById = function (files, id) {
        var _files = this.find_folder(files, function (f) { return f.id === id; });
        if (_files.length > 0)
            return files[0];
        return undefined;
    };
    FilesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], FilesService);
    return FilesService;
}());

//# sourceMappingURL=files.service.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__repetition_service__ = __webpack_require__("../../../../../src/app/desktop/services/repetition.service.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_0__repetition_service__["d"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__repetition_service__["a"]; });
/* unused harmony reexport SequitorOpenFile */
/* unused harmony reexport SequitorPaste */
/* unused harmony reexport SequitorAppendNewline */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pdf_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/pdf-viewer.service.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__pdf_viewer_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__text_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/text-viewer.service.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_2__text_viewer_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__video_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/video-viewer.service.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_3__video_viewer_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__debug_service__ = __webpack_require__("../../../../../src/app/desktop/services/debug.service.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_4__debug_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__files_service__ = __webpack_require__("../../../../../src/app/desktop/services/files.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_5__files_service__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_5__files_service__["b"]; });






//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/pdf-viewer.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PdfViewerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__files_service__ = __webpack_require__("../../../../../src/app/desktop/services/files.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__repetition_service__ = __webpack_require__("../../../../../src/app/desktop/services/repetition.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PdfViewerService = (function () {
    function PdfViewerService(sequitorService) {
        this.sequitorService = sequitorService;
        this.pdf = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({
            name: 'Open a PDF',
            updated: new Date(),
            id: -2,
            type: __WEBPACK_IMPORTED_MODULE_2__files_service__["a" /* FileType */].PDF,
            data: ''
        });
        this.currentPDF = this.pdf.asObservable();
    }
    PdfViewerService.prototype.open = function (file) {
        if (!(file.type === __WEBPACK_IMPORTED_MODULE_2__files_service__["a" /* FileType */].PDF))
            return false;
        this.pdf.next(file);
        // not given folder info ugh...oh well dont need????
        this.sequitorService.addInput(new __WEBPACK_IMPORTED_MODULE_3__repetition_service__["b" /* SequitorOpenFile */](file, file));
        return true;
    };
    PdfViewerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__repetition_service__["d" /* SequitorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__repetition_service__["d" /* SequitorService */]) === "function" && _a || Object])
    ], PdfViewerService);
    return PdfViewerService;
    var _a;
}());

//# sourceMappingURL=pdf-viewer.service.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/repetition.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export EAction */
/* unused harmony export SequitorInput */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SequitorOpenFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SequitorCopy; });
/* unused harmony export SequitorAppendNewline */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SequitorPaste; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return SequitorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__debug_service__ = __webpack_require__("../../../../../src/app/desktop/services/debug.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sequitor__ = __webpack_require__("../../../../../src/app/desktop/services/_sequitor.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_modialog_plugins_js_native__ = __webpack_require__("../../../../ngx-modialog/plugins/js-native/bundle/ngx-modialog-js-native.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__file_selector_service__ = __webpack_require__("../../../../../src/app/desktop/services/file-selector.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EAction;
(function (EAction) {
    EAction[EAction["COPY"] = 0] = "COPY";
    EAction[EAction["PASTE"] = 1] = "PASTE";
    EAction[EAction["OPENFILE"] = 2] = "OPENFILE";
    EAction[EAction["STR_ENTER"] = 3] = "STR_ENTER";
})(EAction || (EAction = {}));
var SequitorInput = (function () {
    function SequitorInput(data, charVal, action) {
        this.data = data;
        this.charVal = charVal;
        this.action = action;
    }
    SequitorInput.prototype.isThisSequitor = function (c) {
        return c === this.charVal;
    };
    return SequitorInput;
}());

var SequitorOpenFile = (function (_super) {
    __extends(SequitorOpenFile, _super);
    function SequitorOpenFile(file, folder) {
        return _super.call(this, { file: file, folder: folder }, 'o', EAction.OPENFILE) || this;
    }
    return SequitorOpenFile;
}(SequitorInput));

var SequitorCopy = (function (_super) {
    __extends(SequitorCopy, _super);
    function SequitorCopy(text, selector, from) {
        var _this = _super.call(this, {
            from: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"],
            selector: selector,
            text: text
        }, 'c', EAction.COPY) || this;
        _this.text = text;
        _this.selector = selector;
        _this.from = from;
        return _this;
    }
    return SequitorCopy;
}(SequitorInput));

var SequitorAppendNewline = (function (_super) {
    __extends(SequitorAppendNewline, _super);
    function SequitorAppendNewline(file) {
        return _super.call(this, { file: file }, 'n', EAction.STR_ENTER) || this;
    }
    return SequitorAppendNewline;
}(SequitorInput));

var SequitorPaste = (function (_super) {
    __extends(SequitorPaste, _super);
    function SequitorPaste(text, selector, to) {
        var _this = _super.call(this, {
            to: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"],
            selector: selector,
            text: text
        }, 'p', EAction.COPY) || this;
        _this.text = text;
        _this.selector = selector;
        _this.to = to;
        return _this;
    }
    return SequitorPaste;
}(SequitorInput));

var SequitorService = (function () {
    function SequitorService(debugService, modal, selectorService) {
        var _this = this;
        this.debugService = debugService;
        this.modal = modal;
        this.selectorService = selectorService;
        this.grammar = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](new __WEBPACK_IMPORTED_MODULE_3__sequitor__["a" /* Rule */]());
        this.input = [];
        this.digramIndex = {};
        this.currentGrammar = this.grammar.asObservable();
        this.selectorService.currentFileAndStatus.subscribe(function (f) {
            if (f.done) {
                _this.repeatOn(f.files);
            }
        });
    }
    SequitorService.prototype.repeatOn = function (files) {
        var _this = this;
        var actions = this.terminals.map(function (t) {
            return _this.input.find(function (action) { return action.charVal === t; });
        });
        var applyable = actions.filter(function (action) { return action; });
        var fns = [];
        var p = applyable.find(function (a) { return a instanceof SequitorPaste; });
        if (!p) {
            return;
        }
        var x = p;
        var destination = x.to;
        files.forEach(function (file) {
            var old = destination.data['text'];
            var n = file.data['title'];
            destination.data['text'] = old + "\n" + n;
        });
    };
    SequitorService.prototype.repeat = function () {
        var _this = this;
        if (!this.applyToFiles.done) {
            return;
        }
        var actions = this.terminals.map(function (c) {
            return _this.input.find(function (input) { return input.charVal === c; });
        });
        console.log('ACTIONS', actions);
    };
    SequitorService.prototype.addInput = function (action) {
        this.input.push(action);
        var inputCharSequence = this.input.map(function (seq) { return seq.charVal; });
        var newGrammar = Object(__WEBPACK_IMPORTED_MODULE_3__sequitor__["b" /* makeGrammar */])(inputCharSequence);
        this.grammar.next(newGrammar);
        this.debugService.write(Object(__WEBPACK_IMPORTED_MODULE_3__sequitor__["d" /* printHTMLGrammar */])(newGrammar));
        console.log('newGrammar', newGrammar);
        var _a = Object(__WEBPACK_IMPORTED_MODULE_3__sequitor__["c" /* printGrammar */])(newGrammar), str = _a.str, ruleSet = _a.ruleSet;
        var lines = str.split('\n');
        var _b = lines[0].split('>'), num = _b[0], expansion = _b[1];
        var symbols = expansion.split(' ');
        var isDigit = function (ch) { return /[0-9]/.test(ch); };
        var maybeRepetition = symbols.findIndex(function (ch) { return isDigit(ch); });
        if (symbols.length > maybeRepetition &&
            symbols[maybeRepetition + 1] === symbols[maybeRepetition]) {
            var terminals = [];
            Object(__WEBPACK_IMPORTED_MODULE_3__sequitor__["e" /* printRuleExpansion */])(ruleSet[maybeRepetition], terminals);
            this.notifyRepetition(terminals);
        }
    };
    SequitorService.prototype.notifyRepetition = function (terminals) {
        // const dialogRef = this.modal
        //   .alert()
        //   .message('Repetition, will automate')
        //   .open();
        this.terminals = terminals;
        this.selectorService.toggle();
        // .alert()
        // .size('lg')
        // .showClose(true)
        // .title('A simple Alert style modal window')
        // .body(
        //   `
        //   <h4>Alert is a classic (title/body/footer) 1 button modal window that
        //   does not block.</h4>
        //   <b>Configuration:</b>
        //   <ul>
        //       <li>Non blocking (click anywhere outside to dismiss)</li>
        //       <li>Size large</li>
        //       <li>Dismissed with default keyboard key (ESC)</li>
        //       <li>Close wth button click</li>
        //       <li>HTML content</li>
        //   </ul>`
        // )
        // .open();
        // dialogRef.result.then(result => alert(`The result is: ${result}`));
    };
    SequitorService.prototype.clearInput = function () {
        // this.sequitor.next(new Rule());
        this.input = [];
    };
    SequitorService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__debug_service__["a" /* DebugService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__debug_service__["a" /* DebugService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ngx_modialog_plugins_js_native__["b" /* Modal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ngx_modialog_plugins_js_native__["b" /* Modal */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__file_selector_service__["a" /* FileSelectorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__file_selector_service__["a" /* FileSelectorService */]) === "function" && _c || Object])
    ], SequitorService);
    return SequitorService;
    var _a, _b, _c;
}());

// change this so that it acts as the whatever stuff.
// function main() {
//   digramIndex = {};
//   const S = new Rule();
//   const input = $('#input')
//     .val()
//     .split('');
//   if (input.length) {
//     S.last().insertAfter(new SeqSymbol(input.shift()));
//   }
//   while (input.length) {
//     S.last().insertAfter(new SeqSymbol(input.shift()));
//     S.last()
//       .getPrev()
//       .check();
//   }
//   $('#output').html(printGrammar(S));
// }
// function printHash() {
//   // tslint:disable-next-line:forin
//   return Object.keys(digramIndex).reduce(
//     (hash, key) => hash + key + ': ' + digramIndex[key] + '\n',
//     ''
//   );
// }
//# sourceMappingURL=repetition.service.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/text-viewer.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TextViewerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__files_service__ = __webpack_require__("../../../../../src/app/desktop/services/files.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TextViewerService = (function () {
    function TextViewerService(filesService) {
        this.filesService = filesService;
        this.text = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"]({
            name: '',
            updated: new Date(),
            id: -2,
            type: __WEBPACK_IMPORTED_MODULE_1__files_service__["a" /* FileType */].TEXT,
            data: { text: '' }
        });
        this.currentTEXT = this.text.asObservable();
    }
    TextViewerService.prototype.open = function (file) {
        if (!(file.type === __WEBPACK_IMPORTED_MODULE_1__files_service__["a" /* FileType */].TEXT))
            return false;
        this.text.next(file);
        return true;
    };
    TextViewerService.prototype.save = function (file) {
        return this.filesService.update(file);
    };
    TextViewerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__files_service__["b" /* FilesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__files_service__["b" /* FilesService */]) === "function" && _a || Object])
    ], TextViewerService);
    return TextViewerService;
    var _a;
}());

//# sourceMappingURL=text-viewer.service.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/services/video-viewer.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoViewerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VideoViewerService = (function () {
    function VideoViewerService() {
    }
    VideoViewerService.prototype.open = function (file) { };
    VideoViewerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], VideoViewerService);
    return VideoViewerService;
}());

//# sourceMappingURL=video-viewer.service.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/text-viewer/text-viewer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/desktop/text-viewer/text-viewer.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-container *ngIf=\"file.id === -2\">\n    <textarea (paste)=\"onPaste($event)\" name=\"content\" id=\"content\" class=\"code w-100 h-75 \"></textarea>\n    <a class=\"fr f6 link pionter ba mt2 bw1 ph3 pv2 mb2 dib black\" href=\"#0\">Save as New File</a>\n</ng-container>\n<ng-container *ngIf=\"file.id > -2\">\n    <textarea (paste)=\"onPaste($event)\" name=\"content\" id=\"content\" class=\"code w-100 h-75 \" [(ngModel)]=\"file.data.text\"></textarea>\n    <div class=\"fr\">\n        <a class=\"f6 link pionter ba mt2 bw1 ph3 pv2 mb2 dib dim gray\" (click)=\"newFile()\">Open New</a>\n        <a class=\"f6 link pionter ba mt2 bw1 ph3 pv2 mb2 dib dim black\" (click)=\"save()\">Save</a>\n    </div>\n</ng-container>"

/***/ }),

/***/ "../../../../../src/app/desktop/text-viewer/text-viewer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TextViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_text_viewer_service__ = __webpack_require__("../../../../../src/app/desktop/services/text-viewer.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/desktop/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_repetition_service__ = __webpack_require__("../../../../../src/app/desktop/services/repetition.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TextViewerComponent = (function () {
    function TextViewerComponent(textViewerService, repetitionService) {
        this.textViewerService = textViewerService;
        this.repetitionService = repetitionService;
    }
    TextViewerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.textViewerService.currentTEXT.subscribe(function (t) { return (_this.file = t); });
    };
    TextViewerComponent.prototype.onPaste = function ($event) {
        this.repetitionService.addInput(new __WEBPACK_IMPORTED_MODULE_3__services_repetition_service__["c" /* SequitorPaste */]($event.text, 'content', this.file));
        console.log($event);
    };
    TextViewerComponent.prototype.save = function () {
        if (this.file !== undefined)
            this.textViewerService.save(this.file);
    };
    TextViewerComponent.prototype.newFile = function () { };
    TextViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-text-viewer',
            template: __webpack_require__("../../../../../src/app/desktop/text-viewer/text-viewer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/desktop/text-viewer/text-viewer.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_text_viewer_service__["a" /* TextViewerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_text_viewer_service__["a" /* TextViewerService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services__["f" /* SequitorService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["f" /* SequitorService */]) === "function" && _b || Object])
    ], TextViewerComponent);
    return TextViewerComponent;
    var _a, _b;
}());

//# sourceMappingURL=text-viewer.component.js.map

/***/ }),

/***/ "../../../../../src/app/desktop/video-viewer/video-viewer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/desktop/video-viewer/video-viewer.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  video-viewer works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/desktop/video-viewer/video-viewer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideoViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VideoViewerComponent = (function () {
    function VideoViewerComponent() {
    }
    VideoViewerComponent.prototype.ngOnInit = function () {
    };
    VideoViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'csc428-video-viewer',
            template: __webpack_require__("../../../../../src/app/desktop/video-viewer/video-viewer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/desktop/video-viewer/video-viewer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], VideoViewerComponent);
    return VideoViewerComponent;
}());

//# sourceMappingURL=video-viewer.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map