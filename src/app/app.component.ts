import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'csc428-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'csc428';
  constructor(private router: Router) {}

  showIntro() {
    return this.router.url === '/';
  }
}
