import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DebugService {
  private output = new BehaviorSubject<string>('');
  private debugValue = new BehaviorSubject<boolean>(false);
  currentOutput = this.output.asObservable();
  currentDebugValue = this.debugValue.asObservable();

  constructor() {}

  toggle(): boolean {
    const next = !this.debugValue.getValue();
    if (next === false) this.output.next('');
    this.debugValue.next(next);
    return next;
  }

  on() {
    this.debugValue.next(true);
  }
  off() {
    this.debugValue.next(false);
    this.output.next('');
  }

  write(nextVal: string) {
    this.output.next(nextVal);
  }
}
