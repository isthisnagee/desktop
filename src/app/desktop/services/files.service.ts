import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export enum FileType {
  FOLDER,
  PDF,
  MOVIE,
  TEXT
}
export type Files = Tree<File>;
export interface File {
  id: number;
  name: string;
  type: FileType;
  updated: Date;
  data?: any;
}

// a Tree with no children is a leaf :)
interface Tree<T> {
  value: T;
  children?: Tree<T>[];
}

@Injectable()
export class FilesService {
  private TEXT: Files[] = [
    {
      value: {
        id: 15,
        name: 'todo',
        updated: new Date(),
        type: FileType.TEXT,
        data: {
          text: `
        Things To Do (ordered)
        ------------

        1. Pay Tuition
        `
        }
      }
    },
    {
      value: {
        id: 16,
        name: 'bear',
        updated: new Date(),
        type: FileType.TEXT,
        data: {
          text: `
           ___
         {~._.~}
          ( Y )
         ()~*~()
         (_)-(_)
        `
        }
      }
    }
  ];
  private MOVIES: Files = {
    value: {
      id: 5,
      name: 'movies',
      updated: new Date(),
      type: FileType.FOLDER
    },
    children: [
      {
        value: {
          id: 11,
          name: 'Halt and Catch Fire',
          updated: new Date(),
          type: FileType.FOLDER
        },
        children: [
          {
            value: {
              id: 6,
              name: 's01e01',
              updated: new Date(),
              type: FileType.MOVIE
            }
          },
          {
            value: {
              id: 7,
              name: 's01e02',
              updated: new Date(),
              type: FileType.MOVIE
            }
          },
          {
            value: {
              id: 8,
              name: 's01e03',
              updated: new Date(),
              type: FileType.MOVIE
            }
          },
          {
            value: {
              id: 9,
              name: 's01e04',
              updated: new Date(),
              type: FileType.MOVIE
            }
          }
        ]
      },
      {
        value: {
          id: 11,
          name: 'Game of Thrones',
          updated: new Date(),
          type: FileType.FOLDER
        },
        children: [
          {
            value: {
              id: 6,
              name: 's08e01',
              updated: new Date(),
              type: FileType.MOVIE
            }
          },
          {
            value: {
              id: 7,
              name: 's08e02',
              updated: new Date(),
              type: FileType.MOVIE
            }
          },
          {
            value: {
              id: 8,
              name: 's08e03',
              updated: new Date(),
              type: FileType.MOVIE
            }
          },
          {
            value: {
              id: 9,
              name: 's08e04',
              updated: new Date(),
              type: FileType.MOVIE
            }
          }
        ]
      }
    ]
  };
  private PDFS: Files = {
    value: { id: 0, name: 'pdfs', updated: new Date(), type: FileType.FOLDER },
    children: [
      {
        value: {
          type: FileType.PDF,
          id: 1,
          name: 'pdf1',
          updated: new Date(),
          data: {
            title: 'title 1',
            body: `
Thought leadership theory of change or low-hanging fruit society a
low-hanging fruit shared unit of analysis social impact. Paradigm;
peaceful collaborative consumption strategize thought leader.
Mobilize, vibrant game-changer technology efficient, optimism,
co-create think tank preliminary thinking grit systems thinking
strategize problem-solvers overcome injustice. Resilient scalable,
humanitarian storytelling empathetic; compelling optimism,
inspirational.

Agile strategy granular, best practices inspire sustainable boots
on the ground, her body her rights social intrapreneurship youth
impact investing optimism parse greenwashing. Thought leader design
thinking; parse citizen-centered dynamic; inspirational; youth
ideate because. Corporate social responsibility ideate sustainable
cultivate inspire change-makers. Empathetic save the world empower
scale and impact program areas disrupt rubric vibrant social return
on investment correlation. Strategize mass incarceration; global
policymaker grit triple bottom line social intrapreneurship
compassion. Commitment program area, bandwidth, innovation
effective altruism game-changer grit policymaker. Empower; our work
empower communities effective issue outcomes agile equal
opportunity boots on the ground inspiring revolutionary think tank.

Collaborative cities granular challenges and opportunities save the
world because outcomes communities. Improve the world, shine, grit
catalyze granular deep dive vibrant. Grit youth, bandwidth,
contextualize; data peaceful, resist collective impact low-hanging
fruit.
           `
          }
        }
      },
      {
        value: {
          type: FileType.PDF,
          id: 2,
          name: 'pdf2',
          updated: new Date(),
          data: {
            title: 'title 2',
            body: `
Leverage program areas collaborative consumption our work social
intrapreneurship; social innovation targeted save the world her
body her rights scale and impact resilient progress radical issue
outcomes. Design thinking thought leader improve the world
sustainable uplift rubric. Thought leader society inspirational
social return on investment low-hanging fruit compassion, silo
fairness vibrant synergy initiative innovate. Contextualize justice
a her body her rights mass incarceration.

Data corporate social responsibility, natural resources collective impact
shine think tank. Do-gooder big data thought provoking, strategize move the
needle accessibility, social innovation overcome injustice white paper triple
bottom line co-create mobilize program areas. To co-create, shared unit of
analysis; big data collective impact living a fully ethical life when.
Optimism strategy leverage inspirational and, segmentation inspiring social
enterprise effective white paper venture philanthropy thought partnership.
Compelling natural resources invest cultivate unprecedented challenge,
cultivate, social intrapreneurship, shared unit of analysis external partners
energize granular relief. Expose the truth program areas change-makers, agile
humanitarian thought leadership social entrepreneur. Think tank activate;
contextualize; human-centered do-gooder.

Overcome injustice equal opportunity, overcome injustice ideate families
effective preliminary thinking; collaborative cities and dynamic ecosystem
B-corp segmentation. Move the needle innovation sustainable social impact
greenwashing. Agile; justice society bandwidth; do-gooder targeted program
area emerging transparent. `
          }
        }
      },
      {
        value: {
          type: FileType.PDF,
          id: 3,
          name: 'pdf3',
          updated: new Date(),
          data: {
            title: 'title 3',
            body: `
Changemaker, systems thinking; equal opportunity social impact
shared unit of analysis do-gooder her body her rights.
Revolutionary, targeted, silo activate, innovation. Efficient the
resistance, think tank changemaker emerging empathetic best
practices optimism replicable. Living a fully ethical life
efficient equal opportunity correlation synergy problem-solvers
targeted expose the truth grit program area targeted a.

Social intrapreneurship empathetic co-creation shine inclusion;
white paper greenwashing, milestones indicators, program area
optimism ecosystem contextualize expose the truth. Academic
technology, technology commitment capacity building white paper
human-centered best practices. Synergy living a fully ethical life
collaborative consumption social innovation movements activate
ideate strategize corporate social responsibility big data
progress. Humanitarian improve the world accessibility innovate
capacity building design thinking. Rubric, change-makers
inspirational; social entrepreneurship rubric grit, social
innovation initiative, radical segmentation social enterprise
technology.

Thought leadership peaceful disrupt policymaker; synergy;
collaborate social entrepreneur innovation inclusive entrepreneur
social capital commitment. Energize design thinking catalyze
inclusion; scalable emerging targeted collaborative cities shared
value collaborative cities overcome injustice agile ecosystem.
Segmentation commitment and strengthening infrastructure;
policymaker catalyze. Black lives matter grit effective; the
transparent youth innovation issue outcomes when theory of change
entrepreneur.
          `
          }
        }
      },
      {
        value: {
          type: FileType.PDF,
          id: 4,
          name: 'cat',
          updated: new Date(),
          data: {
            title: ` CAT(1)`,
            body: `

CAT(1)                    BSD General Commands Manual                   CAT(1) \n

NAME
     cat -- concatenate and print files

SYNOPSIS
     cat [-benstuv] [file ...]

DESCRIPTION
     The cat utility reads files sequentially, writing them to
     the standard output. The file operands are processed in
     command-line order. If file is a single dash ('-') or
     absent, cat reads from the standard input. If file is a UNIX
     domain socket, cat connects to it and then reads it until
     EOF. This complements the UNIX domain binding capability
     available in inetd(8).

     The options are as follows:

     -b      Number the non-blank output lines, starting at 1.

     -e      Display non-printing characters (see the -v option), and
             display a dollar sign ('$') at the end of each line.

     -n      Number the output lines, starting at 1.

     -s      Squeeze multiple adjacent empty lines, causing the output to be single spaced.

     -t      Display non-printing characters (see the -v option), and display tab characters as '^I'.

     -u      Disable output buffering.

     -v      Display non-printing characters so they are visible.
             Control characters print as '^X' for control-X; the delete
             character (octal 0177) prints as '^?'. Non-ASCII characters
             (with the high bit set) are printed as 'M-' (for meta)
             followed by the character for the low 7 bits.

EXIT STATUS
     The cat utility exits 0 on success, and >0 if an error occurs.
`
          }
        }
      }
    ]
  };

  private files: BehaviorSubject<Files> = new BehaviorSubject<Files>({
    value: { id: -1, name: 'home', updated: new Date(), type: FileType.FOLDER },
    children: [this.MOVIES, this.PDFS, ...this.TEXT]
  });
  public currentFiles = this.files.asObservable();

  constructor() {}

  getFoldersAtDepth(depth: number, files = this.files.getValue()) {
    return this._getFoldersAtDepth(files, depth);
  }

  public getChildrenOfFolder(
    folder: File,
    files = this.files.getValue()
  ): File[] {
    const found = (f: File) => f.id === folder.id;
    const tree = this.find_folder(files, found);
    try {
      return tree[0].children.map(t => t.value);
    } catch (error) {
      return [];
    }
  }

  public getParentFolder(ofChild: File, files = this.files.getValue()): File[] {
    return this.find_parent_folder(files, ofChild).children.map(c => c.value);
  }

  private get id() {
    let id = 17;
    return () => {
      id = id + 1;
      return id;
    };
  }

  // functions to add files
  addFile(
    parent: File,
    child: File,
    files: Files = this.files.getValue()
  ): boolean {
    let changed = false;
    const self = this;
    function _bfs(node: Files) {
      const foundParent = node.value.id === parent.id;
      const hasChildren = node.children !== undefined;
      if (foundParent && hasChildren) {
        const newChild = Object.assign({}, child);
        newChild.id = self.id();
        const tree: Files = { value: newChild };
        switch (child.type) {
          case FileType.FOLDER:
            tree.children = [];
            break;
          case FileType.PDF:
            tree.value.data = { title: '', body: '' };
            break;
          case FileType.MOVIE:
            tree.value.data = {};
            break;
          case FileType.TEXT:
            tree.value.data = { text: '' };
            break;
        }
        node.children.push(tree);
        changed = true;
      } else if (hasChildren) {
        node.children.forEach(c => _bfs(c));
      }
    }
    _bfs(files);
    this.files.next(Object.assign({}, files));
    return changed;
  }

  update(file: File, files = this.files.getValue()): boolean {
    let changed = false;
    function _bfs(node: Files) {
      if (node.value.id === file.id) {
        node.value = file;
        node.value.updated = new Date();
        changed = true;
      }
      if (node.children) node.children.forEach(child => _bfs(child));
    }
    _bfs(files);
    this.files.next(files);
    return changed;
  }

  private find_files(files: Files, found: (f: File) => boolean): File[] {
    const foundFiles: File[] = [];
    function _bfs(node: Files) {
      if (found(node.value)) foundFiles.push(node.value);
      if (node.children) node.children.forEach(child => _bfs(child));
    }
    _bfs(files);
    return foundFiles;
  }

  private _getFoldersAtDepth(files: Files, depth: number) {
    const foundFolders: File[] = [];
    function _bfs(node: Files, _depth = 0) {
      if (_depth === depth) foundFolders.push(node.value);
      if (node.children)
        node.children.forEach(child => _bfs(child, _depth + 1));
    }
    _bfs(files);
    return foundFolders;
  }

  private find_folder(files: Files, found: (f: File) => boolean): Files[] {
    const foundFolders: Files[] = [];
    function _bfs(node: Files) {
      if (found(node.value) && node.value.type === FileType.FOLDER)
        foundFolders.push(node);
      if (node.children !== undefined)
        node.children.forEach(child => _bfs(child));
    }
    _bfs(files);
    return foundFolders;
  }

  private find_parent_folder(files: Files, ofChild: File) {
    function _bfs(node: Files) {
      if (
        node.children !== undefined &&
        node.children.find(
          (fileTree: Files) => fileTree.value.id === ofChild.id
        )
      )
        return node;
      if (node.children !== undefined)
        node.children.forEach(child => _bfs(child));
    }
    return _bfs(files);
  }

  /**
   * Returns the first file found with the given name (breadth first)
   * @param name the name of the file
   */
  private findFileByName?(files: Files, name: string): File {
    const _files = this.find_files(files, f => f.name === name);
    if (_files.length > 0) return files[0];
    return undefined;
  }
  private findFolderById?(files: Files, id: number): Files {
    const _files = this.find_folder(files, f => f.id === id);
    if (_files.length > 0) return files[0];
    return undefined;
  }
}
