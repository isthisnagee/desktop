import { Injectable } from '@angular/core';
import { FileType, File, FilesService } from './files.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SequitorService } from './repetition.service';
import { ApplyRepetitionService } from './apply-reptitions.service';

@Injectable()
export class FileSelectorService {
  private hide = new BehaviorSubject<boolean>(true);

  private files = new BehaviorSubject<{ files: File[]; done: boolean }>({
    files: [],
    done: false
  });
  currentHideStatus = this.hide.asObservable();
  currentFileAndStatus = this.files.asObservable();
  constructor(private filesService: FilesService) {}

  toggle() {
    const isHidden = this.hide.getValue();
    this.hide.next(!isHidden);
  }

  fileStart() {
    const { files } = this.files.getValue();
    this.files.next({
      done: false,
      files
    });
  }
  addFile(f: File) {
    const { files, done } = this.files.getValue();
    files.push(f);
    this.files.next({
      done,
      files
    });
  }
  fileFinish() {
    const { files } = this.files.getValue();
    this.files.next({
      done: true,
      files
    });
    // this.applyRepetitionService.repeatOn(files);
    console.log('finish', files);
  }
}
