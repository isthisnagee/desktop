import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FileType, File } from './files.service';
import { SequitorService, SequitorOpenFile } from './repetition.service';
@Injectable()
export class PdfViewerService {
  private pdf = new BehaviorSubject<File>({
    name: 'Open a PDF',
    updated: new Date(),
    id: -2,
    type: FileType.PDF,
    data: ''
  });
  currentPDF = this.pdf.asObservable();

  constructor(private sequitorService: SequitorService) {}

  open(file: File): boolean {
    if (!(file.type === FileType.PDF)) return false;
    this.pdf.next(file);
    // not given folder info ugh...oh well dont need????
    this.sequitorService.addInput(new SequitorOpenFile(file, file));
    return true;
  }
}
