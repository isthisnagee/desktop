import { Component, Injectable, ViewContainerRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { File } from './files.service';
import { DebugService } from './debug.service';

import {
  printRuleExpansion,
  makeGrammar,
  printGrammar,
  printHTMLGrammar,
  Rule
} from './_sequitor';
import { Overlay } from 'ngx-modialog';
import { JSNativeModalModule, Modal } from 'ngx-modialog/plugins/js-native';
import { FileSelectorService } from './file-selector.service';
import { inspectNativeElement } from '@angular/platform-browser/src/dom/debug/ng_probe';

export enum EAction {
  COPY,
  PASTE,
  OPENFILE,
  STR_ENTER
}

export interface ISequitorInput {
  data: any;
  action: EAction;
  charVal: string;
}

export class SequitorInput implements ISequitorInput {
  data: any;
  charVal: string;
  action: EAction;
  constructor(data: any, charVal: string, action: EAction) {
    this.data = data;
    this.charVal = charVal;
    this.action = action;
  }

  isThisSequitor(c) {
    return c === this.charVal;
  }
}

export class SequitorOpenFile extends SequitorInput {
  constructor(file: File, folder: File) {
    super({ file: file, folder: folder }, 'o', EAction.OPENFILE);
  }
}

export class SequitorCopy extends SequitorInput {
  private text: string;
  private selector: string;
  private from: File;
  constructor(text: string, selector: string, from: File) {
    super(
      {
        from: Component,
        selector: selector,
        text: text
      },
      'c',
      EAction.COPY
    );
    this.text = text;
    this.selector = selector;
    this.from = from;
  }
}

export class SequitorAppendNewline extends SequitorInput {
  constructor(file: File) {
    super({ file: file }, 'n', EAction.STR_ENTER);
  }
}

export class SequitorPaste extends SequitorInput {
  private text: string;
  private selector: string;
  to: File;
  constructor(text: string, selector: string, to: File) {
    super(
      {
        to: Component,
        selector: selector,
        text: text
      },
      'p',
      EAction.COPY
    );
    this.text = text;
    this.selector = selector;
    this.to = to;
  }
}

@Injectable()
export class SequitorService {
  private grammar: BehaviorSubject<Rule> = new BehaviorSubject(new Rule());
  input: ISequitorInput[] = [];
  terminals: string[];
  private applyToFiles: { files: File[]; done: boolean };
  digramIndex = {};
  currentGrammar = this.grammar.asObservable();

  constructor(
    private debugService: DebugService,
    public modal: Modal,
    private selectorService: FileSelectorService
  ) {
    this.selectorService.currentFileAndStatus.subscribe(f => {
      if (f.done) {
        this.repeatOn(f.files);
      }
    });
  }
  repeatOn(files: File[]) {
    const actions = this.terminals.map(t =>
      this.input.find(action => action.charVal === t)
    );
    const applyable = actions.filter(action => action);
    const fns = [];
    const p = applyable.find(a => a instanceof SequitorPaste);
    if (!p) {
      return;
    }
    const x = p as SequitorPaste;
    const destination: File = x.to;
    files.forEach(file => {
      const old = destination.data['text'];
      const n = file.data['title'];
      destination.data['text'] = `${old}\n${n}`;
    });
  }
  repeat() {
    if (!this.applyToFiles.done) {
      return;
    }
    const actions = this.terminals.map(c =>
      this.input.find(input => input.charVal === c)
    );
    console.log('ACTIONS', actions);
  }
  addInput(action: ISequitorInput) {
    this.input.push(action);
    const inputCharSequence = this.input.map(
      (seq: ISequitorInput) => seq.charVal
    );

    const newGrammar = makeGrammar(inputCharSequence);
    this.grammar.next(newGrammar);
    this.debugService.write(printHTMLGrammar(newGrammar));

    console.log('newGrammar', newGrammar);
    const { str, ruleSet } = printGrammar(newGrammar);
    const lines = str.split('\n');
    const [num, expansion] = lines[0].split('>');
    const symbols = expansion.split(' ');

    const isDigit = ch => /[0-9]/.test(ch);
    const maybeRepetition = symbols.findIndex(ch => isDigit(ch));
    if (
      symbols.length > maybeRepetition &&
      symbols[maybeRepetition + 1] === symbols[maybeRepetition]
    ) {
      const terminals = [];
      printRuleExpansion(ruleSet[maybeRepetition], terminals);

      this.notifyRepetition(terminals);
    }
  }

  private notifyRepetition(terminals) {
    // const dialogRef = this.modal
    //   .alert()
    //   .message('Repetition, will automate')
    //   .open();
    this.terminals = terminals;
    this.selectorService.toggle();
    // .alert()
    // .size('lg')
    // .showClose(true)
    // .title('A simple Alert style modal window')
    // .body(
    //   `
    //   <h4>Alert is a classic (title/body/footer) 1 button modal window that
    //   does not block.</h4>
    //   <b>Configuration:</b>
    //   <ul>
    //       <li>Non blocking (click anywhere outside to dismiss)</li>
    //       <li>Size large</li>
    //       <li>Dismissed with default keyboard key (ESC)</li>
    //       <li>Close wth button click</li>
    //       <li>HTML content</li>
    //   </ul>`
    // )
    // .open();

    // dialogRef.result.then(result => alert(`The result is: ${result}`));
  }

  clearInput() {
    // this.sequitor.next(new Rule());
    this.input = [];
  }
}
// change this so that it acts as the whatever stuff.
// function main() {
//   digramIndex = {};
//   const S = new Rule();
//   const input = $('#input')
//     .val()
//     .split('');
//   if (input.length) {
//     S.last().insertAfter(new SeqSymbol(input.shift()));
//   }
//   while (input.length) {
//     S.last().insertAfter(new SeqSymbol(input.shift()));
//     S.last()
//       .getPrev()
//       .check();
//   }
//   $('#output').html(printGrammar(S));
// }

// function printHash() {
//   // tslint:disable-next-line:forin
//   return Object.keys(digramIndex).reduce(
//     (hash, key) => hash + key + ': ' + digramIndex[key] + '\n',
//     ''
//   );
// }
