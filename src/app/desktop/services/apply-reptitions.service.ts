import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { File } from './files.service';
import { SequitorService } from './repetition.service';

@Injectable()
export class ApplyRepetitionService {
  private repetitions;
  //   private debugValue = new BehaviorSubject<boolean>(false);
  //   currentOutput = this.output.asObservable();

  constructor() {}

  repeatOn(fs: File[], terminals, inputs) {
    const actions = terminals.map(t =>
      inputs.find(action => action.charVal === t)
    );
    const applyable = actions.filter(action => action);
  }
}
