import { Injectable } from '@angular/core';
import { FileType, File, FilesService } from './files.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class TextViewerService {
  private text = new BehaviorSubject<File>({
    name: '',
    updated: new Date(),
    id: -2,
    type: FileType.TEXT,
    data: { text: '' }
  });
  currentTEXT = this.text.asObservable();

  constructor(private filesService: FilesService) {}

  open(file: File): boolean {
    if (!(file.type === FileType.TEXT)) return false;
    this.text.next(file);
    return true;
  }

  save(file: File): boolean {
    return this.filesService.update(file);
  }
}
