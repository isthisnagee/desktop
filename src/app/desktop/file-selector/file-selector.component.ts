import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  NgZone
} from '@angular/core';
import { Files, FilesService, File, FileType } from '../services/files.service';
import { AfterViewChecked } from '@angular/core/src/metadata/lifecycle_hooks';
import { PdfViewerService } from '../services/pdf-viewer.service';
import { VideoViewerService } from '../services/video-viewer.service';
import { TextViewerService } from '../services/text-viewer.service';
import { FileSelectorService } from '../services/file-selector.service';

@Component({
  selector: 'csc428-file-selector',
  templateUrl: './file-selector.component.html',
  styleUrls: ['./file-selector.component.css']
})
export class FileSelectorComponent implements OnInit {
  hide = true;
  fileTree: Files;
  chain: File[] = [];
  editingFile: File;
  fileType = FileType;
  editRowId: number = -1;
  addNew = false;
  title = 'File Explorer';
  files: File[] = [];
  @ViewChild('selectedInput') selectedInput;
  formFileType = [
    {
      name: 'folder',
      type: FileType.FOLDER
    },
    {
      name: 'pdf',
      type: FileType.PDF
    },
    {
      name: 'text',
      type: FileType.TEXT
    },
    {
      name: 'video',
      type: FileType.MOVIE
    }
  ];
  selectedFiles: File[] = [];

  model: File = {
    id: -2,
    name: '',
    type: FileType.FOLDER,
    updated: new Date()
  };

  submitted = false;

  constructor(
    private zone: NgZone,
    private filesService: FilesService,
    private movieViewer: VideoViewerService,
    private pdfViewer: PdfViewerService,
    private textViewer: TextViewerService,
    private fileSelectorService: FileSelectorService
  ) {
    this.filesService.currentFiles.subscribe(fs =>
      this.zone.run(() => (this.fileTree = fs))
    );
    this.fileSelectorService.currentHideStatus.subscribe(h => (this.hide = h));
  }

  close() {
    this.fileSelectorService.toggle();
    this.fileSelectorService.fileFinish();
  }
  ngOnInit() {
    this.fileSelectorService.fileStart();
    this.getRoot();
    this.goDeeper(this.files[0]);
  }
  onSubmit() {
    console.log(this.fileTree);
    this.filesService.addFile(this.chain[this.chain.length - 1], this.model);
    // reset the views
    // .subscribe(fs => (this.fileTree = fs));
    const currentFolder = this.chain[this.chain.length - 1];
    this.files = this.filesService.getChildrenOfFolder(
      currentFolder,
      this.fileTree
    );
    this.model = {
      id: -2,
      name: '',
      type: FileType.FOLDER,
      updated: new Date()
    };
    this.addNew = false;
  }

  // when clicking away from an object, save :)
  // ngAfterViewChecked() {
  //   if (this.selectedInput) {
  //     this.selectedInput.nativeElement.focus();
  //   }
  // }
  addFile(f: File) {
    if (!this.selectedFiles.find(file => file.id === f.id)) {
      this.selectedFiles.push(f);
      this.fileSelectorService.addFile(f);
    }
  }

  getRoot() {
    this.files = this.filesService.getFoldersAtDepth(0);
  }

  edit(idx: number, file: File) {
    this.editingFile = file;
    this.editRowId = idx;
  }
  noEdit() {
    this.filesService.update(this.editingFile);
    this.editRowId = -1;
  }

  goUpTo(toFolder: File) {
    const indexOfToFolder = this.chain.indexOf(toFolder);
    this.chain = this.chain.slice(0, indexOfToFolder + 1);
    this.files = this.filesService.getChildrenOfFolder(toFolder, this.fileTree);
  }

  goDeeper(fromFolder: File) {
    this.chain.push(fromFolder);
    this.files = this.filesService.getChildrenOfFolder(fromFolder);
  }

  toggleAddFile() {
    this.addNew = !this.addNew;
  }

  openFile(file: File) {
    switch (file.type) {
      case FileType.FOLDER:
        this.goDeeper(file);
        break;
      case FileType.MOVIE:
        this.movieViewer.open(file);
        break;
      case FileType.PDF:
        console.log('opening pdf');
        this.pdfViewer.open(file);
        break;
      case FileType.TEXT:
        this.textViewer.open(file);
        break;
      default:
        break;
    }
  }
}
