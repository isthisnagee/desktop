import {
  Pipe,
  PipeTransform,
  Component,
  OnInit,
  ElementRef,
  Inject,
  ViewContainerRef
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

import {
  File,
  PdfViewerService,
  SequitorService,
  SequitorCopy
} from '../services';

@Component({
  selector: 'csc428-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.css']
})
export class PdfViewerComponent implements OnInit {
  currentPDF: File;
  title: string;
  private dom: Document;
  constructor(
    vcr: ViewContainerRef,
    private sequitorService: SequitorService,
    private pdfViewerService: PdfViewerService,
    @Inject(DOCUMENT) dom: Document
  ) {
    this.dom = dom;
  }

  private copyToClipboard(text) {
    // from: https://www.bennadel.com/blog/3235-creating-a-simple-copy-to-clipboard-directive-in-angular-2-4-9.htm
    let textarea;
    try {
      // In order to execute the "Copy" command, we actually have to have
      // a "selection" in the currently rendered document. As such, we're
      // going to inject a Textarea element and .select() it in order to
      // force a selection.
      // --
      // NOTE: This Textarea is being rendered off-screen.
      textarea = this.dom.createElement('textarea');
      textarea.style.height = '0px';
      textarea.style.left = '-100px';
      textarea.style.opacity = '0';
      textarea.style.position = 'fixed';
      textarea.style.top = '-100px';
      textarea.style.width = '0px';
      this.dom.body.appendChild(textarea);

      // Set and select the value (creating an active Selection range).
      textarea.value = text;
      textarea.select();

      // Ask the browser to copy the current selection to the clipboard.
      this.dom.execCommand('copy');
    } finally {
      // Cleanup - remove the Textarea from the DOM if it was injected.
      if (textarea && textarea.parentNode) {
        textarea.parentNode.removeChild(textarea);
      }
    }
  }
  copy(val: { id: number; val: string }) {
    // const copyText = document.getElementById(`${val.id}`);
    // document.execCommand('Copy');
    const c = new SequitorCopy(val.val, `#${val.id}`, this.currentPDF);
    this.sequitorService.addInput(c);
    this.copyToClipboard(val.val);
  }
  copyTitle() {
    const c = new SequitorCopy(
      this.currentPDF.data.title,
      `title`,
      this.currentPDF
    );
    this.sequitorService.addInput(c);

    this.copyToClipboard(this.currentPDF.data.title);
  }
  tokenize(text: string) {
    if (text === undefined) return [];
    const texts = text.split('\n');
    return texts
      .map(t =>
        t.split(' ').reduce(
          (strs, str, i) => {
            if (str === '\n') {
              strs.push({
                id: -1,
                val: ''
              });
              return strs;
            }
            const lastIdx = strs.length - 1;
            const lastEntry = strs[lastIdx];
            const lastVal = lastEntry.val;
            if (lastVal !== undefined && lastVal.split(' ').length < 2) {
              lastEntry.val = `${lastVal} ${str}`;
              lastEntry.id = i;
              strs[lastIdx] = lastEntry;
              return strs;
            }

            strs.push({
              id: i,
              val: str
            });
            return strs;
          },
          [{ id: -1, val: '' }]
        )
      )
      .reduce((acc, strs) => [...acc, ...strs, { id: -1, val: '' }], []);
  }

  ngOnInit() {
    this.pdfViewerService.currentPDF.subscribe(pdf => (this.currentPDF = pdf));
  }
}
