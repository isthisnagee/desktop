import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgPipesModule } from 'angular-pipes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-modialog';

import { DesktopComponent } from './desktop.component';
import { FileExplorerComponent } from './file-explorer/file-explorer.component';
import { TextViewerComponent } from './text-viewer/text-viewer.component';
import { PdfViewerComponent } from './pdf-viewer/pdf-viewer.component';
import { VideoViewerComponent } from './video-viewer/video-viewer.component';
import { FileSelectorComponent } from './file-selector/file-selector.component';
import { NewFileFormComponent } from './file-explorer/new-file.component';
import {
  FilesService,
  VideoViewerService,
  TextViewerService,
  PdfViewerService,
  SequitorService,
  DebugService
} from './services';
import { FileSelectorService } from './services/file-selector.service';
// import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';

@NgModule({
  imports: [FormsModule, CommonModule, NgPipesModule, NgbModule],
  declarations: [
    DesktopComponent,
    FileExplorerComponent,
    TextViewerComponent,
    PdfViewerComponent,
    VideoViewerComponent,
    FileSelectorComponent,
    NewFileFormComponent
  ],
  bootstrap: [DesktopComponent],
  providers: [
    FilesService,
    VideoViewerService,
    TextViewerService,
    PdfViewerService,
    SequitorService,
    FileSelectorService,
    DebugService
  ]
})
export class DesktopModule {}
