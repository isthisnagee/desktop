import { Component, OnInit } from '@angular/core';
import { DebugService } from './services/debug.service';

@Component({
  selector: 'csc428-desktop',
  templateUrl: './desktop.component.html',
  styleUrls: ['./desktop.component.css']
})
export class DesktopComponent implements OnInit {
  debug: boolean;
  debugText: string;
  constructor(private debugService: DebugService) {}

  ngOnInit() {
    this.debugService.currentOutput.subscribe(t => (this.debugText = t));
    this.debugService.currentDebugValue.subscribe(d => (this.debug = d));
  }
  toggle() {
    this.debugService.toggle();
  }
}
