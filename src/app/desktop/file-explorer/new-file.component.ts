import { Component } from '@angular/core';

import { File, FileType } from '../services';

@Component({
  selector: 'csc428-new-file-form',
  templateUrl: './new-file.component.html',
  styles: [
    `
`
  ]
})
export class NewFileFormComponent {
  fileType = [
    {
      name: 'folder',
      type: FileType.FOLDER
    },
    {
      name: 'pdf',
      type: FileType.PDF
    },
    {
      name: 'text',
      type: FileType.TEXT
    },
    {
      name: 'video',
      type: FileType.MOVIE
    }
  ];

  model: File = {
    id: -2,
    name: '',
    type: FileType.FOLDER,
    updated: new Date()
  };

  submitted = false;

  onSubmit() {
    this.submitted = true;
  }
}
