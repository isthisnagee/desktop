import { Component } from '@angular/core';

export abstract class AppComponent extends Component {
  public abstract getSmall();
  public abstract getLarge();
}
