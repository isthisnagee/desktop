import { Component, OnInit } from '@angular/core';
import { TextViewerService } from '../services/text-viewer.service';
import { File, SequitorService } from '../services';
import { SequitorPaste } from '../services/repetition.service';

@Component({
  selector: 'csc428-text-viewer',
  templateUrl: './text-viewer.component.html',
  styleUrls: ['./text-viewer.component.css']
})
export class TextViewerComponent implements OnInit {
  file: File;
  constructor(
    private textViewerService: TextViewerService,
    private repetitionService: SequitorService
  ) {}

  ngOnInit() {
    this.textViewerService.currentTEXT.subscribe(t => (this.file = t));
  }
  onPaste($event) {
    this.repetitionService.addInput(
      new SequitorPaste($event.text, 'content', this.file)
    );
    console.log($event);
  }

  save() {
    if (this.file !== undefined) this.textViewerService.save(this.file);
  }
  newFile() {}
}
